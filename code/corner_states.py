import time
import sys
import csv

import numpy as np
import matplotlib.pyplot as plt

import Gd
import ipr_pol

sys.path.append('../../chapter-2-thesis/code')
import bott
from utils import Color,timer
import haldane_real as hr
import lattice_r as lr
import tex
import disorder

tex.useTex()

if __name__ == "__main__":
    distance = 0
    Δ_B=5
    Δ_AB=5
    N_lattice=1000
    a=1
    distance=0
    k0=0.05 * 2 * np.pi
    lattice = lr.generate_hex_hex_stretch(N_lattice)
    lattice = disorder.introduce_disorder(lattice,0.07)
        
    M = Gd.create_matrix_TE(lattice, Δ_B, Δ_AB, k0)
    w, v = np.linalg.eig(M)

    idx = w.argsort()[::-1]
    w = w[idx]
    v = v[:, idx]
    ω = 7

    f = -np.real(w)/2

    for i,x in enumerate(f):
        if np.abs(x) < 12:
            print(i,x)
            ipr_pol.intensity_map(v, lattice, i, f[i])
    
