\chapter{Toward a Photonic Topological Anderson Insulator}
\minitoc
\label{sec:chapter4}

\begin{tikzpicture}[remember picture,overlay]
    \node[opacity=1,inner sep=300pt] at ([yshift=-3cm] current page.north) {\includegraphics[trim={0cm 9cm 0cm 9cm},clip,width=\paperwidth,height=\paperheight,keepaspectratio]{../figs/illus_chapter_4.pdf}};
\end{tikzpicture}

\noindent Most of the results presented in this chapter have been
published in \cite{nous} and \cite{skip23}.

\section*{Résumé en Français}
%% \foreignlanguage{french}{

Dans ce chapitre, nous étudions l'influence du désordre dans le
système étudié au chapitre précédent. Le désordre brise la symétrie
par translation ne permettant pas l'étude d'un système infini en
utilisant le théorème de Bloch. L'étude du diagramme de bandes est
ainsi remplacé par l'étude de la densité d'états et le calcul du
nombre de Chern par le calcul de l'indice de Bott.

Nous commençons par rappeler au lecteur quelques propriétés du système
étudié, qui a été introduit de manière plus complète aux
\blue{chapitres} \ref{sec:chapter2b} et \ref{sec:chapter3}. Des atomes
immobiles sont placés dans un réseau en nid d'abeilles avec un
paramètre de maille $ a $ dans le plan $ xy $. Ces atomes sont
assimilés à des diffuseurs ponctuels car leur taille est très petite
devant la longueur d'onde des ondes lumineuses qui résonnent avec la
transition atomique. Chaque atome est assimilé à un système à deux
niveaux, le niveau excité est trois fois dégénéré et l'énergie
nécessaire pour passer de l'état fondamental à l'état excité est
$\hbar \omega_0 $. Le temps de vie pour la désexcitation est quant à
lui donné par l'inverse de la constante $ \Gamma_0 $. Un champ
magnétique transverse au plan $ xy $ peut lever la dégénérescence de
l'état excité par effet Zeeman. On considère que le réseau en nid
d'abeilles peut se décomposer en deux sous-réseaux triangulaires $A$ et
$B$ pour lesquels les fréquences de résonances peuvent différer de la
fréquence $ \omega_0 $.

Comme nous l'avons observé au sein du \blue{chapitre} \ref{sec:chapter3}, les
paramètres $ \Delta_{\textbf{B}} $ et $ \Delta_{AB} $ représentent
respectivement l'intensité du champ magnétique et le décalage dans les
fréquences de résonances entre les sous-réseaux $A$ et $B$. Ces deux
paramètres s'avèrent capables d'ouvrir des gaps autour de la fréquence
de résonance $ \omega_0 $. Le premier par brisure de la symétrie par
renversement du temps, et le deuxième par brisure de la symétrie
d'inversion. La largeur de ce gap a été minutieusement étudiée au
\blue{chapitre} \ref{sec:chapter3} et nous ne faisons dans ce chapitre que confirmer les
résultats obtenus précédemment. Dans ce chapitre comme dans le
précédent, nous faisons aussi le choix de n'étudier que les modes TE ;
en effet, ils sont découplés des modes TM qui ne sont pas affectés par
le champ magnétique et donc moins intéressants à étudier.

%% Nous étendons ensuite un outil déjà introduit au \blue{chapitre}
%% \ref{sec:chapter2} sur le modèle d'Haldane : l'IPR pour \say{Inverse
%%   Participation Ratio}. Cette quantité peut être calculée pour un mode
%% et donne rapidement une idée de l'étalement du mode : un IPR élevé
%% signifie que le mode est localisé dans une région de l'espace, à
%% l'inverse, un faible IPR indique un mode qui s'étale sur tout
%% l'échantillon. Nous pouvons aussi afficher l'intensité du mode sur
%% chaque site afin de voir où se situe le mode précisément. Cela permet
%% que le désordre introduit des états dans le gap. Ces états peuvent se
%% situer aussi bien sur les bords que dans le volume de
%% l'échantillon. Il est montré plus loin que les états de bords peuvent
%% résister au désordre.

Pour confirmer le caractère topologique du gap ouvert par $
\Delta_{\textbf{B}} $, nous reprenons l'indice de Bott introduit au
\blue{chapitre} \ref{sec:chapter2} sur le modèle d'Haldane et nous traçons
le diagramme de phase dans le plan $ (\Delta_{\textbf{B}},
\Delta_{AB}) $ qui se trouve parfaitement correspondre au diagramme de
phase établi à l'aide du nombre de Chern dans le \blue{chapitre}
\ref{sec:chapter3}. L'indice de Bott peut se calculer dans un réseau
périodique ou non. Cela en fait donc un outil de choix pour l'étude de
la topologie dans notre système désordonné. D'autre part, l'indice de
Bott peut être calculé pour une fréquence donnée $ \omega $ ; bien
sûr, il n'est proprement défini que si $ \omega $ appartient à un
gap dans le spectre. Mais on se rend compte en calculant l'indice de Bott pour toutes
les fréquences que celui-ci ne prend des valeurs différentes de zéro
que dans le gap d'énergie ou bien de mobilité. Cela fait de l'indice
de Bott un outil fiable pour caractériser le comportement topologique
dans un système de taille finie.

Ensuite, le désordre est introduit. Le choix qui a été fait ici est
d'introduire le désordre sur les positions des atomes. Chaque atome est
déplacé dans une direction aléatoire de $ \delta r_m $ qui est compris
entre $0$ et $Wa$. Ce choix diffère d'un désordre couramment rencontré
qui est le désordre d'Anderson qui correspondrait aux fréquences
$\omega_0$ aléatoires. Ici comme les atomes sont par construction,
identiques, cela n'aurait pas beaucoup de sens. Il apparaît très vite
que le désordre est en mesure de fermer le gap ouvert par $
\Delta_{\textbf{B}} $ ou $ \Delta_{AB} $ pour des valeurs de désordre
autour de 25\% du pas de réseau. Pour mettre en évidence la
persistance de ce gap face au désordre et la résistance des états de
bord, nous calculons l'indice de Bott ainsi que la densité d'états, le
temps de vie du mode et l'IPR dans le plan $ (\omega, W) $. L'étude
est approfondie dans le cas d'un gap topologique où l'on étudie
également l'impact de la brisure de symétrie.

Finalement, nous finissons par la description d'un phénomène qui
représente l'un des résultats les plus importants de cette thèse :
l'isolant topologique d'Anderson (TAI). Pour cela, nous nous plaçons
dans une situation sans gap mais où la symétrie par inversion du temps
et la symétrie par renversement du temps sont brisées. Typiquement
pour $|\Delta_{\vec{B}}|\approx|\Delta_{AB}|\ne 0$. En introduisant
cette fois le désordre uniquement dans les positions d'atomes du
sous-réseau $B$, nous constatons l'apparition d'une région dans
l'espace de paramètres ou l'indice de Bott est différent de zéro. À
l'aide des autres outils introduits (IPR, DOS, temps de vie du mode),
nous montrons que cette région correspond bel et bien à un gap
topologique de mobilité. Il est ensuite possible de prouver que cette
phase topologique est reliée aux phases topologiques présentes dans
les cas où $ | \Delta_{\textbf{B}} | > | \Delta_{AB} | $ en absence de
désordre. Nous montrons que le désordre permet à une phase topologique
d'exister pour des valeurs de $ W $ allant de 7 \% à 35 \% ; après
cela, la phase topologique disparaît de nouveau.
\newpage

\section{A two-dimensional honeycomb atomic lattice of finite size}

We consider a honeycomb lattice in the $xy$ plane with lattice spacing
$a$ consisting of a large but finite number of atoms $N$. This is in
contrast to \cref{sec:chapter3} where $N$ has been assumed infinite.
The atoms are considered immobile at positions $\vec{r}_m = \left(
x_m, y_m \right)$ numbered from 1 to $N$. The elementary cell of our
lattice consists of two atoms, which we consider as belonging to two
triangular sublattices as in \cref{sec:chapter3}. We call these two
triangular sublattices $A$ and $B$, and we assume that the resonance
frequencies of the atoms in sublattice $A$, denoted by $\omega_A$, may
differ from the resonance frequencies in sublattice $B$, denoted by
$\omega_B$. Each atom is treated as a simplified two-level system with
the total angular momentum in the ground state $J_g = 0$ and in the
excited state $J_e = 1$. The excited state is triply degenerate. This
simplified model is illustrated by \cref{fig:2level}.

\begin{figure}[h]
\centering
  \resizebox{0.5\columnwidth}{!}{\includegraphics{../figs/2level.pdf}}
  \caption{\label{fig:2level}Each atom is treated as a simplified
    two-level system with the total angular momentum in the ground
    state $J_g = 0$ and in the excited state $J_e = 1$. The degeneracy
    of the excited state is lifted by a magnetic field. The energy
    difference between states $|g\rangle$ and $|e\rangle$ is $\hbar
    \omega_0$ and the decay time of the excited state is $1 /
    \Gamma_0$.}
\end{figure}

We then consider, like in \cref{sec:chapter3}, our system placed in a
magnetic field $\mathbf{B}$ directed along the $z$ axis, perpendicular
to the $xy$ plane. We remind here the effective Hamiltonian, a $2 N
\times 2 N$ matrix consisting of $2 \times 2$ blocks $H_{mn}$
describing the interaction between atoms $m$ and $n$ (see
\cref{eq:H_eff}):
\begin{eqnarray}
  H_{mn} & = & \delta_{mn} ((i \pm 2 \Delta_{AB}) \mathds{1}_2 + 2 \sigma_z
  \Delta_{\mathbf{B}}) \nonumber\\
  & - & \frac{6 \pi}{k_0} (1 - \delta_{mn}) \hat{d}_{\tmop{eg}} \mathcal{G}
  (\mathbf{r}_m - \mathbf{r}_n) \hat{d}_{\tmop{eg}}^{\dagger} 
\end{eqnarray}
where $\Delta_{AB} = (\omega_B - \omega_A) / 2 \Gamma_0$ is the
frequency detuning between $A$ and $B$ sublattices in units of the
decay rates of the excited states (assumed identicals for atoms $A$
and $B$). The ``$+$'' or ``$-$'' stands for atoms $A$ and $B$ and
$\Delta_{\mathbf{B}} =g_e \mu_B |\vec{B}| / \Gamma_0$ is the Zeeman shift induced
by the magnetic field $\mathbf{B}$ with $\mu_B$ the Bohr
magneton and $g_e$ the Landé factor. Note that this quantity is also expressed in units of
$\Gamma_0$ so that in the end all the frequencies we compute from
this matrix are going to be in units of $\Gamma_0$. For more details
see \cref{eq:def_freq,eq:def_gamma}.

I now draw the reader's attention to the various global shapes our lattice can take.
An hexagonal shape provides equivalent ``armchair'' type edges and
avoids the asymmetry issues encountered for a rectangular sample. Indeed, as
shown in \cref{fig:samples}, a rectangular lattice presents three different types
of edges depending on how the hexagons are \say{cut}: \say{armchair}, \say{zigzag},
or \say{bearded}. In the following, we will consider both types of lattices
since we do not have a definition of the Bott index for a lattice other than the
rectangular one.

We also note from \cref{fig:samples} that we will distinguish two
zones in our sample: the bulk and the edge. The bulk is defined by a
circular region excluding a certain number of atomic layers near
edges, $N_{\tmop{edge}}=4$ in the following.

\begin{figure}[h]
  \centering
  \resizebox{0.49\columnwidth}{!}{\includegraphics{../figs/fig_honeycomb.pdf}}\resizebox{0.399\columnwidth}{!}{\includegraphics{../figs/lattice.pdf}}
  \caption{\label{fig:samples}(Left) Honeycomb lattice of two-level
    atoms. Atoms $A$ (red) and $B$ (blue) form a unit cell. We split
    the finite-size lattice into a circular bulk region (shaded in
    grey) and the remaining edge regions; the narrowest part of the
    edge has a width of $N_{\tmop{edge}}$ atomic layers. For the
    purpose of illustration, the lattices shown are much smaller than
    the actual ones used in the calculations. Time-reversal symmetry
    can be broken by a magnetic field $\mathbf{B}$ perpendicular to
    the $xy$ plane of the lattice. Inversion symmetry can be broken by
    distinguishing between atoms $A$ and $B$. Disorder is introduced
    by displacing atoms by random distances $\delta r_m \in [0, Wa]$ in
    random directions. (Right) Honeycomb lattice cut in a shape or a
    rectangle to reveal the three types of edges: zigzag,
    armchair, and bearded. The yellow wavy lines indicate that
    interactions occur through exchange of photons, highlighting that it
    is no longer a tight-binding model.}
\end{figure}

\section{Density of states and band gaps}

Once the effective Hamiltonian has been written, it is straightforward to
extract its eigenvalues $\Lambda_m$ and eigenvectors $\bm{\psi}_m$:
\begin{align}
\Lambda_m = -2\left(\frac{\omega_m-\omega_0}{\Gamma_0}\right) + i \frac{\Gamma}{\Gamma_0}  
\end{align}
The density of states (DOS) that counts the number of states in a unit
frequency interval around a given frequency $\omega$ is:
\begin{equation}
  \mathrm{DOS}(\omega) = \sum_{m = 1}^N \delta (\omega - \omega_m)
\end{equation}
By plotting the DOS, see \cref{fig:dos_ipr}, we can observe that
similarly to the previous Chapter, symmetry breaking by inversion and
time reversal symmetry both open gaps around $\omega = \omega_0$. For
$\Delta_{AB} \neq 0$ and $\Delta_{\vec{B}}=0$, we expect to
observe a gap without any edge states. This is indeed the case if we
consider a hexagonal sample shape with armchair edges. Furthermore, if
we break time reversal symmetry using a magnetic field
$\Delta_{\vec{B}}\neq 0$, $\Delta_{AB}=0$, we find that states appear
in the gap. By examining these states, we realize that they are
confined at the edges of the sample. To determine degree of
localization of a state we use its inverse participation ratio. We
extend the definition given in \cref{sec:chapter2}, to take into
account the polarization of the state:
\begin{equation}
  \tmop{IPR}_m = \frac{\sum_{i = 1}^N \left( \sum_{\sigma = \pm 1} | \psi_{m
  \sigma}^{(i)} |^2 \right)^2}{\left( \sum_{i = 1}^N \sum_{\sigma = \pm 1} |
  \psi_{m \sigma}^{(i)} |^2 \right)^2}
\end{equation}
where $\psi_{m \sigma}^{(i)}$ is the amplitude of the mode $m$ with
polarization $\sigma$ on atom $i$. \cref{fig:dos_ipr}
\hyperref[fig:dos_ipr]{$(a)$} shows the DOS for both trivial and
topological gaps and a zoom on the gap is in
\hyperref[fig:dos_ipr]{$(c)$}. In \hyperref[fig:dos_ipr]{$(b)$} and
\hyperref[fig:dos_ipr]{$(d)$} we picked up two modes situated inside
the frequency band gap and localized at the edges. One of them is
even localized at the corners of the sample.

\begin{figure}[h]
  \centering
  \resizebox{0.9\columnwidth}{!}{\includegraphics{../figs/dos_ipr.pdf}}
  \caption{$(a)$ DOS for both trivial (red) and topological (blue) cases
    and a $(c)$ zoom on the gap. Both DOS are similar except in the
    gap where edge states arise in the topological case. $(b)$ and
    $(d)$ show intensities of two modes situated inside the frequency
    band gap and localized at the edges of the sample. The mode in
    $(d)$ is even localized at the corners of the
    sample.\label{fig:dos_ipr}}
\end{figure}

The width of the gap in \cref{fig:dos_ipr} matches the
results obtained in \cref{sec:chapter3}, it suggests that the physical
phenomena in a system of finite size may be similar to those in the
infinite lattice. The question that arises is whether topological
effects will persist despite a finite system size. As we saw in
\cref{sec:chapter2}, in the Haldane model, the Bott index allows us to
identify the topological phase in a system as small as thirty
sites. Therefore, we attempt to identify the topological phase using
the Bott index.

\section{Computing the Bott index: a topological marker}

The Bott index has already been defined in \cref{sec:chapter2}, see
\cref{eq:bott_index}. We will not give its definition again, but we
recall that it is an integer that identifies a topological phase when
it takes a value different from $0$. This index is calculated for a
given frequency by considering modes with frequencies below $\omega$
in \cref{eq:W_for_bott} or summing over all such modes in
\cref{eq:proj_for_bott}: $C_{\Beta} (\omega)$. The system studied in
this Chapter differs substantially from the Haldane model, and is not
formally the framework in which the Bott index is properly defined.
Indeed, our system does not have periodic boundary conditions, and the
Hamiltonian that represents it is not Hermitian. We will see that this
does not preclude the use of $C_{\Beta}$ to characterize topological
phases in our system. To partially account for the non-Hermiticity, we
replace the projector in the definition of $C_{\Beta}$ by a pseudo
projector:
\begin{align}
P = \sum_{i|E_i\leq E_{\tmop{gap}}} |\bm{\psi}_{R,i}\rangle \langle\bm{\psi}_{L,i}|
\end{align}
where $|\bm{\psi}_{R,i}\rangle$ and $|\bm{\psi}_{L,i}\rangle$ stand for right and left
eigenvectors of $H$ respectively. We start by comparing the Bott index, in a
system without disorder to the Chern number calculated in the
previous Chapter. The result is shown in \cref{fig:bott_chern}. The
correspondence is nearly perfect, with discrepancies appearing only
around $|\Delta_{\mathbf{B}}| = |\Delta_{AB}|$. This is not surprising
because the gap can become narrower than the level spacing for
$|\Delta_{\mathbf{B}}| \approx |\Delta_{AB}|$. This correspondence
reassures us that although the use of the Bott index is not fully
justified for our system, we obtain the same result as for the Chern
number, which has already been extensively studied by physicists and
mathematicians, and by us in the previous Chapter. Therefore, we can
make the assumption that the Bott index is a faithful marker of the
topological phase in our atomic system.

\begin{figure}[h]
  \centering
  \resizebox{1\columnwidth}{!}{\includegraphics{../figs/chern_bott.pdf}}
  \caption{Comparison of two topological markers: $(a)$ Chern number
    and $(b)$ Bott index. The phase diagram illustrates the interplay
    of TRS and inversion symmetry breaking denoted respectively
    $\Delta_{\vec{B}}$ and $\Delta_{AB}$. The match between the two
    phase diagrams appears to be almost perfect except around
    $|\Delta_{AB}|=|\Delta_{\vec{B}}|$, where the gap can become
    narrower than the level spacing and the calculation of $C$ is
    compromised. \label{fig:bott_chern}}
\end{figure}

Before introducing disorder, I would like to draw the reader's attention to a
subtlety in the definition of the Bott index. I recall here the final formula
of \cref{sec:bott_index} that allows calculating this index:
\begin{align}
  C_{\Beta} (\omega)  = & \frac{1}{2 \pi i} \tmop{Tr} (\log (UVU^{- 1} V^{-
  1}))\\
   \approx & \frac{1}{2 \pi i} \tmop{Tr} (\log (UVU^{\dagger} V^{\dagger}))\label{eq:bott_dagger}
\end{align}
In the literature \cite{Loring2019Jul}, it is shown that for a sample
sufficiently large, one has $UU^\dagger \approx \mathds{1}_n$ and
$VV^\dagger \approx \mathds{1}_n$, this justifies the use of
\cref{eq:bott_dagger} in the following. However as illustrated by
\cref{fig:bott_inv_dagger}, differences between the two definitions do
exist for our system, at least when $N$ is small. The differences are,
however, only quantitative and will become less and less significant
for increasing $N$. In this Chapter we use the definitions with the
daggers and we take into account of the non-Hermiticity using
$|\bm{\psi}_{R,i}\rangle$ and $|\bm{\psi}_{L,i}\rangle$ in the definition of the
projector. Note also that I gave two definitions for $U$ and $V$ in
\cref{sec:chapter2}, one using a transition matrix names $W$ and the
other using the projector $P$, the last one is used throughout this
Chapter.

\begin{figure}[h]
  \centering
  \resizebox{0.45\columnwidth}{!}{\includegraphics{../figs/map_disorder_bott_120_inv.pdf}}
  \resizebox{0.45\columnwidth}{!}{\includegraphics{../figs/map_disorder_bott_120_dagger.pdf}}
  \caption{\label{fig:bott_inv_dagger}Comparison between the
    calculation of the Bott index using the formula with the inverses
    on the left and the daggers on the right. Average performed over
    50 realizations for $N=120$ atoms with $\Delta_{\mathbf{B}} = 5$
    and $\Delta_{AB} = 0$. Visible differences appear.}
\end{figure}

\section{Introducing disorder }

\begin{figure}[h]
  \centering
  \resizebox{0.95\columnwidth}{!}{\includegraphics{../figs/fig_lattices.pdf}}
  \caption{\label{fig:disorder_illus}$(a)$ A rectangular lattice used for the
  calculation of Bott index. The lattice used for the calculations in this
  Chapter consists of $N = 2244$ atoms and is as close as possible to a square.
  $(b)$ A lattice having the shape of a hexagon with armchair edges that do
  not support edge states in the absence of magnetic filed (i.e., for
  $\Delta_{\mathbf{B}} = 0$) The lattice used for the calculations in this
  Chapter consists of $N = 4326$ atoms. The edge of the lattice is defined by
  $h_{\tmop{edge}} = (N_{\tmop{edge}} - 1) a \sqrt{3} / 2$ and the remaining
  grey part is referred to as ``bulk''. We use $N_{\tmop{edge}} = 4$ in this
  Chapter.}
\end{figure}

We now come to one of the most interesting parts of this work: the
study of the combined effect of disorder and topology. The simplest
way to introduce disorder is to randomly modify atomic transition
frequencies, referred to as Anderson disorder in reference to the
famous 1958 article \cite{Anderson1958Mar}. This ``on-site'' disorder
has the advantage of being simple to introduce and represents physical
situations that actually exist. For example, if we consider a network
of resonators with wavelengths in the microwave domain, such as
ceramic cylinders in the experiment by \cite{Reisner2021Mar}, there
will certainly be fluctuations of resonance frequency of each cylinder
because it depends on the cylinder size, the purity of the material,
etc. However, in an atomic system, all atoms are exactly the same, so
that on-site disorder does not naturally appear in an experiment. We
then turn to another type of disorder: disorder in the atomic
positions. Position of each atom is perturbed by a small quantity
$\delta \mathbf{r}_m$, with $| \delta \mathbf{r}_m | \in [0 ; Wa]$. Thus,
each atom is randomly displaced within a circle of radius $Wa$ around
its initial position. This way of introducing disorder is illustrated
in \cref{fig:disorder_illus}. Note that from an experimental point of
view, this setup can be achieved by trapping atoms in a 2D optical
speckle potential superimposed on a regular intensity distribution.

In the following we fix the lattice spacing $a = \lambda_0 / 20$ so
that we have $k_0 a = 2 \pi \times 0.05$. This value is chosen to
facilitate comparison with previous results, see \cite{Perczel2017Jul}
and \cref{sec:chapter3} in the absence of disorder. In the previous
Chapter, we have shown that the topological gap closes for a too high value
of $k_0 a$.

\subsection{Disorder as a topological killer}

\cref{fig:bott_5_0} illustrates a scenario where disorder destroys the
topological phase created by application of the external magnetic
field. In \cref{fig:bott_5_0} we have chosen values of
$\Delta_{\mathbf{B}} = 5$ and $\Delta_{AB} = 0$. The topological phase
is destroyed for a disorder strength of $W\approx 0.25$. The destruction of
the topological phase is evident from the average Bott index, which
transitions from $- 1$ to $0$. By plotting the profile $\langle
C_{\Beta} \rangle = - 0.5$, we can observe that this average value of
the Bott index delineates a region where states become scarcer.

Additionally, we can observe that the values of IPR in the region
where $\langle C_{\Beta}\rangle \ne 0$ are significantly different from
zero in the bulk, indicating that these states are localized. At the
same time, discrete modes arise at the edges of the sample. It is worth
noting that the spacing of these edge modes is not arbitrary; we have:

\begin{equation}
  \frac{\delta \omega}{\Gamma_0} \propto \frac{1}{\sqrt{N}}
\end{equation}
%% A numerical analysis with $\Gamma_0=2\pi\times6$Mhz gives
%% $\delta\omega/\Gamma_0\approx 0.87$ which is observed in
%% \cref{fig:bott_5_0}.
The impact of disorder on these modes can be explained by a
perturbative approach, the dashed lines \cref{fig:bott_5_0}
\hyperref[fig:bott_5_0]{$(b)$} representing the trajectories of the
mode frequencies in the $(W, \omega)$ space. Unfortunately, this
calculation does not allow for an analytical calculation of the
Bott index, which is why I have not included it here. Interested
reader can find the details of the calculations in \cite{nous}.

\begin{figure}[t]
  \centering
  \resizebox{0.9\columnwidth}{!}{\includegraphics{../figs/fig50.pdf}}
  \caption{\label{fig:bott_5_0}Closing of a topological band gap by
    disorder. (a) Average Bott index $\langle C_{\Beta} \rangle$ as a
    function of disorder strength $W$ for \ $\Delta_{\mathbf{B}} = 5$
    and $\Delta_{AB}$ = 0. The white contour reproduced for reference
    in all plots $(a)-(d)$ shows the level $\langle C_{\Beta} \rangle$
    = \nonconverted{minus} 0.5. $(b)$ Average edge DOS. Dashed lines
    show edge modes predicted by the perturbation theory.  Frequencies
    of the lowest and highest frequency edge modes traced up to higher
    values of $W$ approximate band edges. $(c)$ Average bulk IPR. Deep
    violet color corresponds to either very low IPR or DOS = 0 (no
    modes). $(d)$ Average quasimode decay rate in units of the natural
    decay rate $\Gamma_0$ of an isolated atom. $\langle
    C_{\Beta}\rangle$ is calculated for the square sample of
    \cref{fig:disorder_illus}\blue{$(a)$} and DOS and $\Gamma$ $-$ for
    the hexagonal sample of
    \cref{fig:disorder_illus}\blue{$(b)$}. Averaging is performed over
    $50-200$ realizations.}
\end{figure}

Another aspect to consider when investigating topological effects is
the lifetime of states. Indeed, the Hamiltonian we consider here is
non-Hermitian because the system can emit energy into the free space
surrounding it. It is observed that the decay rates given by $\Gamma_n
= \Gamma_0 \tmop{Im} (\Lambda_n)$ are higher for edge states with
frequencies in the gap, indicating that the lifetimes of states are
shorter. This is expected and can be explained quite simply: the
states in the gap are located at the edge and are therefore more
likely to escape into the vacuum than those located in the bulk. Of
course, this should complicate their experimental observations as
these states would decay rapidly.

\begin{figure}[h]
  \centering
  \resizebox{0.9\columnwidth}{!}{\includegraphics{../figs/figipr.pdf}}
  \caption{\label{fig:dab_ipr}Impact of topological properties on
    spatial localization of modes. Mean bulk IPR in the vicinity of
    topologically trivial gaps for $(a)\; \Delta_{\mathbf{B}} = 0,
    \Delta_{AB} = 5$ and $(b)$ $\Delta_{\mathbf{B}} = 5, \Delta_{AB} =
    10$.}
\end{figure}

To investigate the impact of the topological nature of the gap on
Anderson localization of modes in the bulk, we display the IPR in the
bulk for several different situations (see \cref{fig:bott_5_0},
\cref{fig:dab_ipr} and \cref{fig:ipr_18}). To open a gap of a specific
width, one can proceed in three different ways: by breaking
time-reversal symmetry ($|\Delta_{\mathbf{B}}| \neq 0$,$|\Delta_{AB}|
= 0$), by breaking the inversion symmetry of sites
($|\Delta_{\mathbf{B}}| = 0$,$|\Delta_{AB}| \neq 0$), or by breaking
both ($|\Delta_{\mathbf{B}}| \neq 0$,$|\Delta_{AB}| \neq 0$). The
corresponding IPRs are shown in \cref{fig:bott_5_0,fig:dab_ipr}.


It is important to remember that usually, breaking time-reversal
symmetry makes disorder-induced modes less localized
\cite{Evers2008Oct}. However, this is not observed here as
demonstrated by blue and green curves in \cref{fig:ipr_18} where
breaking TRS (blue line) leads to a higher IPR, and thus stronger
localization, than in the situation with TRS (green line).

On the other hand, when the system exhibits a topological gap: a
situation depicted by the red curve in \cref{fig:ipr_18}, IPR is
smaller and thus localization is weaker than in the trivial situation.



%% It is observed that the
%% breaking of TRS, that make a topological gap, results in less Anderson
%% localization in the bulk. This is coherent with the literature on the
%% subject. Generally, the breaking of time-reversal symmetry leads to a
%% decrease in the IPR, not an increase as explained in
%% \cite{Evers2008Oct}, however, the strongest localization are achieved
%% for opposite since the maximum IPR is reached for $\Delta_{\mathbf{B}}
%% = 5$ and $\Delta_{AB} = 10$, which indeed corresponds to a breaking of
%% time-reversal symmetry. The topological nature of our band gap reduces
%% IPR and thus increases the localization length of disorder-induced
%% modes inside the band gap.


%% It is observed that the maximum IPR is higher (by about a factor of 3)
%% when inversion symmetry is strongly broken ($\Delta_{AB} = 10$).  This
%% means that in the presence of inversion symmetry breaking, the edge
%% states tend to exist less sharply at the edges and also tend to be
%% located within the bulk. This is further highlighted by
%% \cref{fig:ipr_18}, which directly shows the IPR for a chosen disorder
%% of W=18\% and several situations where both symmetries are
%% broken.

%% This analysis should be considered in light of results from research
%% on Anderson localization \cite{Evers2008Oct}. }. Here, 
%% \begin{equation}
%%   \xi = \frac{a}{\sqrt{\tmop{IPR}}}
%% \end{equation}
\begin{figure}[h]
  \centering
  \resizebox{0.5\columnwidth}{!}{\includegraphics{../figs/figipr18.pdf}}
  \caption{\label{fig:ipr_18}Sections of \cref{fig:bott_5_0} (c) (red), \cref{fig:dab_ipr}
  (a) (green), and \cref{fig:dab_ipr} (b) (blue) at a given disorder $W = 0.18$,
  averaged over 2000 realizations of disorder. Error bars show the standard
  error of the mean. Vertical dashed lines show band gap edges.}
\end{figure}

\begin{figure}[h]
  \centering
  \resizebox{0.99\columnwidth}{!}{
    \includegraphics{../figs/imap_10.1.pdf}
    \includegraphics{../figs/imap_10.5.pdf}
  }
  \caption{Edge states localized at the corners of the hexagon for
    $\Delta_{\textbf{B}} = \Delta_{AB} = 5$. On the left, we have the
    case without disorder for $(\omega-\omega_0)/\Gamma_0 = 10.1$, and
    on the right, we added disorder $W=0.07$, causing the frequency of
    the state to shift to $(\omega-\omega_0)/\Gamma_0 = 10.5$. %% The
    %% simulation was conducted for $N=1000$.
\label{fig:corner_state}}
\end{figure}

\begin{figure}
  \centering
  \resizebox{0.9\columnwidth}{!}{\includegraphics{../figs/fig55.pdf}}
  \caption{\label{fig:tai_1}Opening of a topological pseudogap by disorder. The
  four plots show the same quantities as \cref{fig:bott_5_0} but for
  $\Delta_{\mathbf{B}} = \Delta_{AB} = 5$ and disorder in positions of atoms $B$ only.}
\end{figure}

\begin{figure}
  \centering
  \resizebox{0.9\columnwidth}{!}{\includegraphics{../figs/fig_bott.pdf}}
  \caption{\label{fig:tai_2}Average Bott index $\langle C_{\Beta} \rangle$
  calculated for four different values of $\Delta_{\mathbf{B}}$ around
  $\Delta_{\mathbf{B}} = \Delta_{AB} = 5$ for $a = \lambda_0 / 20$ in a
  rectangular sample shown in \cref{fig:disorder_illus}. The white contour shows the
  boundary $\langle C_{\Beta} \rangle = - 0.5$ between the topologically
  nontrivial region where \ $\langle C_{\Beta} \rangle = - 1$ (violet) and the
  trivial region where \ $\langle C_{\Beta} \rangle = 0$ (red). Ensemble
  averaging is performed over 60 independent realizations of disorder for each
  graph.}
\end{figure}
\begin{figure}
  \centering
  \resizebox{0.9\columnwidth}{!}{\includegraphics{../figs/fig_dosipr.pdf}}
  \caption{\label{fig:tai_ipr_dos}Average full $(a)$, bulk $(b)$ and
    edge $(c)$ DOS in a hexagon-shaped sample shown in
    \cref{fig:disorder_illus} \hyperref[fig:disorder_illus]{$(b)$} for
    $a = \lambda_0 / 20$, $\Delta_{AB} = 5$ and $\Delta_{\mathbf{B}} =
    4$. Average IPR of bulk modes is shown in $(d)$. Ensemble
    averaging is performed over 200 independent realizations of
    disorder for each graph. The white contour in $(a - d)$ shows the
    boundary of the topologically nontrivial region from
    \cref{fig:tai_2} \hyperref[fig:tai_2]{$(c)$}.}
\end{figure}

\begin{figure}
  \centering
  \resizebox{0.9\columnwidth}{!}{\includegraphics{../figs/fig_3d.pdf}}
  \caption{\label{fig:phase_diagram_3d}$(a)$ Phase diagram of the 2D,
    disordered honeycomb atomic lattice in the 3D parameter space $(W,
    \omega, \Delta_{\mathbf{B}})$ for $a = \lambda_0 / 20$ and
    $\Delta_{AB} = 5$. The blue surface shows the boundary between the
    topologically nontrivial (inside, $\langle C_{\Beta} \rangle = -
    1$) and trivial (outside,$\langle C_{\Beta} \rangle = 0$) regions
    of the parameter space. The orange plane $\Delta_{\mathbf{B}} =
    \Delta_{AB}$ separates the TI phase for $\Delta_{\mathbf{B}} >
    \Delta_{AB}$ from TAI phase for $\Delta_{\mathbf{B}} <
    \Delta_{AB}$. $(b)$ 3D density plot of average bulk IPR with the
    orange plane separating TI and TAI as in (a). For clarity, the
    values less than $0.006$ are not shown (transparent).}
\end{figure}




\subsection{The emergence of a topological Anderson insulator}

We now come to one of the most interesting results of this work: the
emergence of a topological Anderson insulator (TAI) in our system. The
concept of TAI was introduced in \cref{sec:chapter1}, but I will
provide a brief intuitive definition here: we consider a system to
exhibit a TAI when a topological phase emerges due to the introduction
of disorder into a topologically trivial system. This behavior is
quite surprising and atypical. It arises from collective effects. As we
have observed so far in this Chapter, disorder mostly has the effect
of closing the gap and destroying the associated topological phase. Therefore,
it is unexpected that it could play the opposite role.

To achieve TAI, we start from a situation without a gap and introduce
disorder in the hope of observing the emergence of a topological
phase. The Bott index serves as a marker; we calculate it as we have
done so far to identify the topological phase more or less easily. It
is worth noting that the use of the Bott index is somewhat unusual and
delicate because we calculate the Bott index in situations where there
is no energy gap. However, similarly to Chern number, the calculation
of Bott index is still meaningful in the presence of a mobility
gap. Therefore, it is a suitable marker.

We choose $\Delta_{\mathbf{B}} = 5$ and $\Delta_{AB} = 5$ to avoid
having a gap. Then, we introduce disorder in atomic positions as we
did previously, and check whether $C_{\Beta}$ becomes different from
zero. Unfortunately, $C_{\Beta}$ remains equal to zero for all
frequencies signaling that this way of introducing disorder does not
lead to TAI. Indeed, it is likely that this way of introducing
disorder destroys any structure in the system too rapidly and thus
kills any chance of observing a topological phase. The honeycomb
structure is an essential component for inducing a topological phase
in this system. Therefore, we choose to only partially disturb the
system by displacing only the atoms of $B$ sublattice. By doing so, we
observe that an ``island'' where $C_{\Beta}\ne 0$ appears for disorder
values ranging from 7\% to 35\% in \cref{fig:tai_1}
\hyperref[fig:tai_1]{$(a)$}. It turns out that the edge modes are
concentrated in this region, as highlighted by the plot of edge DOS
\cref{fig:tai_1} \hyperref[fig:tai_1]{$(b)$}. At the same time, modes
localized in the bulk appear for disorder values ranging from 20\% to
30\%. Note that in contrast to the naïve expectation following from
what is known about localization of states due to disorder, all states
are not localized in our 2D system. This is due to the vector nature
of light and longitudinal fields \cite{Maximo2015Dec}.

%% This contrasts with the physics of Anderson localization, which
%% suggests that localized modes exist in a certain frequency band
%% regardless of the level of disorder. Despite both TAI and Anderson
%% localization sharing the name ``Anderson,'' they are nevertheless two
%% different situations with their own conditions of existence and
%% properties.

Before going further into the conditions for the existence of TAI,
let's focus on an unexpected behavior of our system. By examining
figures \cref{fig:bott_5_0} and \cref{fig:tai_1}, we observe edge
states outside the region marked as topological by the Bott index. The
existence of these modes can be due to two phenomena: firstly, the
magnetic field can indeed create edge states outside of the gap
region, at almost any frequency. However, unlike the modes within the
gap, these modes, are close in frequency to bulk modes. For this
reason, they do not benefit from topological protection against
disorder, as the slightest perturbation can couple them to bulk modes
with similar frequencies, unlike the modes within the gap which
persist up to 25\% disorder in \cref{fig:bott_5_0}.



Another reason for the appearance of edge modes outside of the region
where $\langle C_{\Beta}\rangle\ne 0$ is the corners of the finite-size
sample that we consider. Indeed, we observe modes localized in the
corners because the armchair edge shape is broken there. We show one of
these corner states in \cref{fig:corner_state}. These states exist, of
course, in the absence of disorder and resist up to a disorder of
about 10\%. Another anomaly caused by these extremely localized modes
is shown in \cref{fig:tai_1}, where for $(\omega - \omega_0) /
\Gamma_0\approx 10$, we observe a region where $\langle C_{\Beta}
\rangle =-1 $.  One might then wonder if these modes are the trace of
second-order topology \cite{Benalcazar2017Jul,Schindler2018Jun}, but
this question is not addressed in this thesis.

We now focus more extensively on the characterization of TAI. This is
to provide experimentalists with a more precise idea of where to
look. For this purpose, we vary the value of $\Delta_{\mathbf{B}}$
around the point $\Delta_{\mathbf{B}} = \Delta_{AB} = 5$, see
\cref{fig:tai_2}. For $\Delta_{\textbf{B}} > \Delta_{AB}$, we observe
a topological band gap already for $W=0$, which reduces in size and
then closes when we increase disorder, whereas for
\ $\Delta_{\textbf{B}} < \Delta_{AB}$, we have a gap that is initially
trivial but becomes topological due to disorder for $W$ in a
particular range. Finally, it can be observed that for too small value
of \ $\Delta_{\textbf{B}} \leq 3$, the topological phase disappears
completely.

Let's look at \cref{fig:tai_ipr_dos} for the DOS and IPR in the case
of \ $\Delta_{\textbf{B}} = 4 < \Delta_{AB} = 5$. The transition
between the trivial gap and the topological gap containing edge states
is clearly visible in the edge DOS. Bulk IPR shows that localized
states appear in the bulk for frequencies within the topologically
nontrivial region of the parameter space.

We can even go further and instead of plotting the contour $\langle
C_{\Beta} \rangle = - 0.5$ in the $(W, \omega)$ plane, we can plot it
in the $(W, \omega, \Delta_{AB})$ space for a given value of
$\Delta_{AB}$, see \cref{fig:phase_diagram_3d}. In this way, we highlight
that the topological phase emerging due to disorder is connected to
the topological phase arising in the absence of disorder due to the
breaking of TRS. The fact that these two phases are connected is not
surprising and has already been established for QSH insulators
\cite{Prodan2011May, Yamakage2013May}.

