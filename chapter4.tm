<TeXmacs|2.1>

<style|<tuple|generic|british>>

<\body>
  <doc-data|<doc-title|Toward a Photonic Topological Anderson Insulator>>

  The results presented in this chapter have mostly been published in [] and
  [].

  <section|Our system: A two-dimensional honeycomb atomic lattice>

  We consider a honeycomb lattice in the <math|x*y> plane with parameter
  <math|a> consisting of <math|N\<ggg\>1> atoms. Typically, to observe all
  the effects discussed here, <math|N> should be of the order of 1000. For a
  larger system, the calculations quickly become unmanageably long, as we
  will see later. These atoms are considered immobile at positions
  <math|r<rsub|m>=<around*|(|x<rsub|m><space|1em>y<rsub|m>|)>> \ numbered
  from 1 to <math|N>. The elementary cell of our lattice consists of two
  atoms, which we consider as belonging to two triangular sublattices. We
  call these two triangular sublattices <math|A> and <math|B>, and we assume
  that the resonance frequencies of the atoms in sublattice <math|A>, denoted
  by <math|\<omega\><rsub|A>>, may differ from the resonance frequencies in
  sublattice <math|B>, denoted by <math|\<omega\><rsub|B>>. Each atom is
  treated as a simplified two-level system with the total angular momentum in
  the ground state <math|J<rsub|g>=0> and in the excited state
  <math|J<rsub|e>=1>. The excited state is triply degenerate. This simplified
  model is illustrated by <reference|2level>

  <\big-figure|<image|figs/2level.pdf|0.5par|||>>
    <label|2level>Each atom is treated as a simplified two-level system with
    the total angular momentum in the ground state <math|J<rsub|g>=0> and in
    the excited state <math|J<rsub|e>=1>. The excited state is triply
    degenerate. The energy required for the excitation is
    <math|\<hbar\>\<omega\><rsub|0>> and the decay time is
    <math|1/\<Gamma\><rsub|0>>.
  </big-figure>

  We then consider our system placed in a magnetic field <math|<math-bf|B>>
  directed along the <math|z> axis, perpendicular to the <math|x*y> plane.
  This magnetic field lifts the degeneracy of the excited state through the
  Zeeman effect (see Chapter 1). It is important to note that the
  interactions between atoms occur via the electromagnetic field, so this is
  not a tight-binding model as in Chapter 2. Also, unlike Chapter 4, we treat
  the system here as finite, in real space. Finally, we take care to
  distinguish between two similar notations denoting distinct concepts:
  <math|B> for the <math|B> sites and <math|<math-bf|B>> for the magnetic
  field.\ 

  We also choose to focus only on TE modes, those polarized in the plane, as
  they are decoupled from the TM modes, which can be studied separately but
  are not of great interest here because they are not affected by the
  magnetic field. Taking into account all these restrictions, we can deduce
  from the Hamiltonian presented in Chapter 3 an effective
  <math|2N\<times\>2N> matrix consisting of <math|2\<times\>2> blocks
  <math|H<rsub|m*n>> describing the interaction between atoms <math|m> and
  <math|n>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|H<rsub|m*n>>|<cell|=>|<cell|\<delta\><rsub|m*n><around*|(|<around*|(|i\<pm\>2\<Delta\><rsub|A*B>|)><with|font|Bbb|1><rsub|2>+2\<sigma\><rsub|z>\<Delta\><rsub|<math-bf|B>>|)>>>|<row|<cell|>|<cell|->|<cell|<frac|6\<pi\>|k<rsub|0>><around*|(|1-\<delta\><rsub|m*n>|)>d<rsub|eg>\<cal-G\><around*|(|<math-bf|r><rsub|m>-<math-bf|r><rsub|n>|)>d<rsub|eg><rsup|\<dagger\>><eq-number>>>>>
  </eqnarray*>

  where we introduced <math|\<Delta\><rsub|A*B>=<around*|(|\<omega\><rsub|B>-\<omega\><rsub|A>|)>/2\<Gamma\><rsub|0>>
  the frequency detuning between <math|A> and <math|B> sublattices in unit of
  the decay rates of the excited states (assumed identicals for atoms
  <math|A> and <math|B>). The \P<math|+>\Q

  or \P<math|->\Q stands for atoms <math|A> and <math|B> and
  <math|\<Delta\><rsub|<math-bf|B>>=\<mu\><rsub|B>B/\<Gamma\><rsub|0>> is the
  Zeeman shift induced by the magnetic field <math|<math-bf|B>> with
  <math|\<mu\><rsub|B>> the bohr magnetron. Note that this quantity is also
  expressed in unity of <math|\<Gamma\><rsub|0>> so that in the end all the
  frequency we will compute from this matrix are going to be in unity of
  <math|\<Gamma\><rsub|0>>. We also remind the reader that
  <math|<with|font|Bbb|1><rsub|2>> is the <math|2\<times\>2> identity matrix
  and <math|\<sigma\><rsub|z>> is the third Pauli matrix:

  <\equation>
    \<sigma\><rsub|z>=<matrix|<tformat|<table|<row|<cell|1>|<cell|0>>|<row|<cell|0>|<cell|-1>>>>>
  </equation>

  <math|\<cal-G\><around*|(|<math-bf|r>|)>> is the dyadic Green's function
  established in Chapter 3 []. For the convenience of the reader I remind it
  here:

  <\equation>
    \<cal-G\><around*|(|<math-bf|r>|)>=-<frac|\<mathe\><rsup|i*k<rsub|0>r>|4\<pi\>r><around*|(|P<around*|(|i*k<rsub|0>r|)><with|font|Bbb|1>+Q<around*|(|i*k<rsub|0>r|)><frac|<math-bf|r<strong|>>\<otimes\><math-bf|r>|r<rsup|2>>|)>
  </equation>

  with <math|k<rsub|0>=\<omega\><rsub|0>/c=2\<pi\>/\<lambda\><rsub|0>>,
  <math|\<omega\><rsub|0>=<around*|(|\<omega\><rsub|A>+\<omega\><rsub|B>|)>/2>,
  <math|P<around*|(|x|)>=1-1/x+1/x<rsup|2>> and
  <math|Q<around*|(|x|)>=-1+3/x-3/x<rsup|2>>. Note that this expression
  describes the propagation of electromagnetic wave in the basis of linear
  polarizations, to make things easier for us, we tranform it into the basis
  of circular polarization so we have to add
  <math|\<Delta\><rsub|<math-bf|B>>> and <math|\<Delta\><rsub|A*B>> only on
  the diagonal:

  <\equation>
    d<rsub|eg>=<frac|1|<sqrt|2>><matrix|<tformat|<table|<row|<cell|1>|<cell|i>>|<row|<cell|-1>|<cell|i>>>>>
  </equation>

  I now draw the reader's attention to the various shapes our lattice can
  take. Of course, a hexagonal shape provides equivalent ``armchair'' type
  edges and avoids the asymmetry issues encountered in a rectangular sample.
  Indeed, as shown in <reference|samples>, a rectangular lattice presents
  three different types of edges depending on how the hexagons are ``cut":
  ``armchair,'' ``zigzag,'' or ``bearded.'' In the following, we will
  consider both types of lattices since we do not have a definition of the
  Bott index for a lattice other than a rectangular one.

  We also note from <reference|samples> that we will distinguish two zones in
  our sample: the bulk and the edge. The bulk is defined by a circular region
  with a radius of approximately 80% of that of the hexagonal cell.

  <\big-figure|<image|figs/fig_honeycomb.pdf|0.49par|||><image|figs/lattice.pdf|0.399par|||>>
    <label|samples>(Left) Honeycomb lattice of two-level atoms. Atoms
    <math|A> (red) and <math|B> (blue) form a unit cell. We split the
    finite-size lattice into a circular bulk region (shaded in grey) and the
    remaining edge regions; the narrowest part of the edge has a width of
    <math|N<rsub|edge>> atomic layers. For the purpose of illustration, the
    lattices shown are much smaller than theactual ones used in the
    calculations. Time-reversal symmetry can be broken by a magnetic field
    <math|<math-bf|B>> perpendicular to the <math|x*y> plane of the lattice.
    Inversion symmetry can be broken by distinguishing between atoms <math|A>
    and <math|B>. Disorder is introduced by displacing atoms by random
    distances <math|\<delta\>r<rsub|m>\<in\><around*|[|0,W|]>> in random
    directions. (Right) Honeycomb lattice divided into rectangle to reveal
    the three types of edges: Zigzag, Armchair, and Bearded. The yellow wavy
    lines indicate that interactions occur through the magnetic field,
    indicating that it is no longer a tight-binding model.
  </big-figure>

  <section|Density of states and band gaps>

  Once our effective Hamiltonian has been written, it is straightforward to
  extract its eigenvalues <math|\<lambda\><rsub|m>> and eigenvectors
  <math|\<psi\><rsub|m>>. We can then establish the connection between the
  physical system and our mathematical representation using the following
  formulas:

  <\eqnarray*>
    <tformat|<table|<row|<cell|-<frac|1|2>Re<around*|(|\<lambda\><rsub|m>|)>>|<cell|=>|<cell|<dfrac|\<omega\><rsub|m>-\<omega\><rsub|0>|\<Gamma\><rsub|0>><eq-number>>>|<row|<cell|Im<around*|(|\<lambda\><rsub|m>|)>>|<cell|=>|<cell|<dfrac|\<Gamma\><rsub|m>|\<Gamma\><rsub|0>><eq-number>>>>>
  </eqnarray*>

  We can define the density of states (DOS) that counts the number of states
  for a given frequency <math|\<omega\>> by:

  <\equation>
    D<around*|(|\<omega\>|)>=<big|sum><rsub|i=1><rsup|N>\<delta\><around*|(|\<omega\>-\<omega\><rsub|i>|)>
  </equation>

  By plotting the Density of States (DOS), we can observe that similarly to
  the previous chapter, symmetry breaking by inversion and time reversal
  symmetry both open gaps around <math|\<omega\>=\<omega\><rsub|0>>. For
  <math|\<Delta\><rsub|AB>\<neq\>0>, we expect to observe a gap without any
  edge states. This is indeed the case if we choose a hexagonal lattice with
  armchair edges; otherwise, we always have edge states due to the asymmetry
  of the edges. Furthermore, if we break time reversal symmetry using a
  magnetic field, we do observe a region with fewer states, but it still
  contains some. By examining these states, we realize that they are situated
  at the edges of the sample. It is sometimes said that these states are
  ``localized'' at the edges. However, this terminology is delicate because
  ``localized'' implies that there would be a spatial restriction to the
  presence of the function. However, an edge state is anything but spatially
  restricted since it is ``spread out'' over the size of the sample. To avoid
  misunderstandings, I will simply state in the rest of this manuscript that
  edge states are \Plocated at the edges\Q.

  The criterion we have chosen to determine where the edge states are located
  is to use the inverse participation ratio of the mode. Unlike the
  definition given in Chapter 2, we must take into account the polarizations;
  other than that, it is the same concept. It is calculated from the
  coefficients of the eigenvector associated with the mode.\ 

  <\equation>
    IPR<rsub|m>=<frac|<big|sum><rsub|i=1><rsup|N><around*|(|<big|sum><rsub|\<sigma\>=\<pm\>1><around*|\||\<psi\><rsub|m\<sigma\>><rsup|<around*|(|i|)>>|\|><rsup|2>|)><rsup|2>|<around*|(|<big|sum><rsub|i=1><rsup|N><big|sum><rsub|\<sigma\>=\<pm\>1><around*|\||\<psi\><rsub|m\<sigma\>><rsup|<around*|(|i|)>>|\|><rsup|2>|)><rsup|2>>
  </equation>

  Alternatively, we can skip the summation and simply display the absolute
  values of the coefficients on each site, as shown in <reference|dos_ipr>.

  <\big-figure|<image|figs/dos_ipr.pdf|0.9par|||>>
    <label|dos_ipr>
  </big-figure>

  The excellent correspondence between the density of states presented here
  and the band diagram derived in the previous chapter suggests that the
  physics will be similar. The question that arises is whether topological
  effects will persist despite a significantly reduced system size. As we saw
  in Chapter 2, in the Haldane model, the Bott index allows us to identify
  the topological phase in a model with barely twenty sites. Therefore, we
  will attempt to locate the topological phase and its resistance to disorder
  using this index.

  <section|Computing the Bott index: a topological marker>

  We now come to one of the most interesting parts of this work: the study of
  the combined effect of disorder and topology. The Bott index has already
  been defined in Chapter 2. We will not give its definition again, but we
  recall that it is an integer that identifies a topological phase when it
  takes the values <math|1> or <math|-1>. This index is calculated for a
  given frequency, denoted as <math|C<rsub|\<Beta\>><around*|(|\<omega\>|)>>,
  and identifies the topological phase. It can be seen that the system
  studied in this chapter differs from the Haldane model, which takes us out
  of the framework in which the Bott index is properly defined. Indeed, our
  system here does not have periodic boundary conditions, and the Hamiltonian
  that represents it is not Hermitian; energy leaks are possible in the
  direction of the <math|z>-axis. We will start by comparing it, in a system
  without disorder, to the Chern number calculated in the previous chapter.
  This result is shown in <reference|bott_chern>. The correspondence is
  nearly perfect, with only the lines for which
  <math|\<Delta\><rsub|<math-bf|B>>=\<Delta\><rsub|A*B>> showing differences.
  This is not surprising because in these regions, the gap closes or is very
  small, which can make it impossible to define a topological index. This
  correspondence reassures us that although our Bott index is not perfectly
  defined for this system, we obtain the same result as for the Chern number,
  which has already been extensively studied by physicists and
  mathematicians, and by us in the previous Chapter. Therefore, we can make
  the assumption that by adding disorder, our Bott index will continue to be
  a faithful marker of the topological phase.

  <\big-figure|<image|figs/chern_bott.pdf|1par|||>>
    <label|bott_chern>
  </big-figure>

  Before introducing disorder, I would like to draw the reader's attention to
  a subtlety in the definition of the Bott index. I recall here the final
  formula defined in [] that allows calculating this index.

  <\eqnarray*>
    <tformat|<table|<row|<cell|C<rsub|\<Beta\>><around*|(|\<omega\>|)>>|<cell|=>|<cell|<frac|1|2\<pi\>i>Tr<around*|(|log<around*|(|U*V*U<rsup|-1>V<rsup|-1>|)>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2\<pi\>i>Tr<around*|(|log<around*|(|U*V*U<rsup|\<dagger\>>V<rsup|\<dagger\>>|)>|)>>>>>
  </eqnarray*>

  \ In the literature, these two formulas are used equivalently; indeed, it
  can be mathematically proven that there is a homotopy between
  <math|U<rsup|-1>> and <math|U<rsup|\<dagger\>>> and that the use of the
  Hermitian conjugate or the inverse does not change the result. However, in
  this specific case, this is not what I have observed, as shown in
  <reference|bott_inv_dagger>, for a system with a number of sites relatively
  small. One reason that may explain this difference is that the Bott index
  is not perfectly defined for this system, and we lose the equivalence
  between the two. In doubt, I strongly recommend using the formula with the
  inverses because it is the ``true'' formula; the other is only an
  approximation if the system does not perfectly match the requirement of the
  defintiion of the Bott index. The computing time saved is not very
  significant anyway compared to the time required for diagonalization, which
  is the real bottleneck of this calculation.

  Note also that because of its definition, the Bott index is always an
  integer, this has been proven in Chapter 2 and constitutes a good way of
  checking our calulation. Any non-integer Bott index should be treated with
  the utmost caution. In the following most of the maps present Bott index
  averaged over several realisations of disorder, of course in these cases, a
  continuous range of values can be observed.

  <\big-figure|<image|figs/map_disorder_bott_120_inv.pdf|0.45par|||>
  <image|figs/map_disorder_bott_120_dagger.pdf|0.45par|||>>
    <label|bott_inv_dagger>Comparison between the calculation of the Bott
    index using the formula with the inverses on the left and the daggers on
    the right. Average performed over 50 realizations for 120 sites with
    <math|\<Delta\><rsub|<math-bf|B>>=5> and <math|\<Delta\><rsub|A*B>=0>.
    For such a small numbers of sites, visible differences appear.
  </big-figure>

  <section|Introducing disorder >

  <\big-figure|<image|figs/fig_lattices.pdf|0.95par|||>>
    <label|disorder_illus><math|<around*|(|a|)>> A rectangular lattice used
    for the calculation of Bott index. The lattice used for the calculations
    in this work consists of <math|N=2244> atoms and is as close as possible
    to a square. <math|<around*|(|b|)>> A lattice having the shape of a
    hexagon with armchair edges that do not support edge states in the
    absence of magnetic filed (i.e., for <math|\<Delta\><rsub|<math-bf|B>>=0>)
    The lattice used for the calculations in this work consists of
    <math|N=4326> atoms. The edge of the lattice is defined by
    <math|h<rsub|edge>=<around*|(|N<rsub|edge>-1|)>a<sqrt|3>/2> and the
    remaining grey part is referred to as \Pbulk\Q. We use
    <math|N<rsub|edge>=4> in this work.
  </big-figure>

  We will now see how to introduce disorder into our system. A first approach
  that has been used many times is to introduce disorder directly on the
  sites themselves, referred to as Anderson disorder in reference to the
  famous 1958 article []. This disorder, also known as ``on-site'' disorder,
  has the advantage of being simple to introduce and represents physical
  situations that actually exist. For example, if we consider a network of
  resonators with wavelengths in the microwave domain, such as ceramic posts
  as in the experiment by Mortessagne [], there will certainly be uncertainty
  about the resonance frequency of each post because it depends on the size
  of the post, the purity of the material, etc. However, this approach is not
  ours because we have not observed anything concrete using this disorder. We
  then turn to another type of disorder: disorder in the positions. Each
  position of each site will be perturbed by a small quantity
  <math|\<delta\><math-bf|r>>, with <math|<around*|\||\<delta\><math-bf|r>|\|>\<in\><around*|[|-W;W|]>>.
  Thus, our site is randomly displaced within a circle of radius <math|W>
  around its initial position. This way of introducing disorder is illutrated
  in <reference|disorder_illus>. As can be seen in the figure, despite this
  disorder, the Bott index continues to take non-zero values for more than
  15% disorder for values of <math|\<Delta\><rsub|<math-bf|B>>=5> and
  <math|\<Delta\><rsub|A*B>> = 0 see <reference|bott_5_0>. However, this is
  not conclusive evidence of the existence of a topological gap. The Bott
  index could take random values in a gapless region since it is defined only
  within a gap. But we have already seen in the region without disorder that
  the Bott index seems to be zero outside the gap. To prove the existence of
  a topological gap, we need to calculate the DOS in this region and analyze
  the IPR of the modes.\ 

  Note that in the following we fix the lattice spacing as
  <math|a=\<lambda\><rsub|0>/20> so that we have
  <math|k<rsub|0>a=2\<pi\>\<times\>0.05>. This value is chosen to be in
  accordance with the results presented in []. In the previous chapter, a
  study was conducted and showed that the topological gap closes for a too
  high value of <math|k<rsub|0>a>.

  <subsection|Disorder as a topological killer>

  <reference|bott_5_0> illustrates a scenario where disorder destroys the
  topological phase created by the magnetic field. Here, we have chosen
  values of <math|\<Delta\><rsub|<math-bf|B>>=5> and
  <math|\<Delta\><rsub|A*B>=0>. The topological phase is destroyed for a
  disorder strength of <math|W>=25%. This destruction of the topological
  phase is evident from the average Bott index, which transitions from
  <math|-1> to <math|0>. By plotting the profile
  <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-0.5>, we can
  observe that this average value of the Bott index delineates a region where
  states become scarcer.

  \ Additionally, we can observe that the values of IPR in the region
  delimited by the non-zero Bott index are close to zero or zero in the bulk,
  indicating that these states are located on the edges of the sample. As we
  have seen throughout this thesis, the presence of edge states resistant to
  disorder is one of the main characteristics of topological phases. It is
  worth noting that the spacing of these disorder-resistant modes is not
  arbitrary; we have:

  <\equation>
    \<delta\>\<omega\>=<frac|1|<sqrt|N<rsub|sites>>>
  </equation>

  Being inversely proportional to the perimeter confirms their edge
  character. The impact of disorder on these modes can be explained by a
  pertrubative approach, the dashed lines plotted represent the trajectories
  of the modes in the <math|<around*|(|W,\<omega\>|)>> space, as well as the
  gap-closing region. Nevertheless, these calculations did not lead to an
  analytical calculation of the Bott index, which is why I have not included
  them here. Interested readers can find the details of the calculations in
  [].

  <\big-figure|<image|figs/fig50.pdf|0.9par|||>>
    <label|bott_5_0>Closing of a topological band gap by disorder. (a)
    Average Bott index <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>>
    as a function of disorder strength <math|W> for
    \ <math|\<Delta\><rsub|<math-bf|B>>=5> and <math|\<Delta\><rsub|A*B>> =
    0. The white contour reproduced for reference in all plots
    <math|<around*|(|a|)>-<around*|(|c|)>> shows the level
    <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>> = \<minus\>0.5.
    <math|<around*|(|b|)>> Average edge DOS. Dashed lines show edge modes
    predicted by the perturbation theory. Frequencies of the lowest and
    highest frequency edge modes traced up to higher values of <math|W>
    approximate band edges. <math|<around*|(|c|)>> Average bulk IPR. Deep
    violet color corresponds to either very low IPR or DOS = 0 (no modes).
    <math|<around*|(|d|)>> Average quasimode decay rate in units of the
    natural decay rate <math|\<Gamma\><rsub|0>> of an isolated atom.
    <math|N<rsub|sites>=2244> averaged over <math|50-200> realizations.
  </big-figure>

  Another aspect to consider when investigating topological effects is the
  lifetime of states. Indeed, the Hamiltonian we consider here is
  non-Hermitian because the system can emit energy into the free space
  surrounding it. It is observed that the decay rates given by
  <math|\<Gamma\><rsub|n>=\<Gamma\><rsub|0>
  Im<around*|(|\<lambda\><rsub|n>|)>> are higher in the gap, indicating that
  the lifetimes of states are shorter. This difference is expected and can be
  explained quite simply: the states in the gap are located on the edge and
  are therefore more likely to escape into the vacuum than those located in
  the bulk. Of course, this complicates experimental observations as these
  states disappear rapidly.

  <\big-figure|<image|figs/figipr.pdf|0.9par|||>>
    <label|dab_ipr>Impact of topological properties on spatial localization
    of modes. Mean bulk IPR in the vicinity of topologically trivial gaps for
    <math|<around*|(|a|)> \<Delta\><rsub|<math-bf|B>>=0,\<Delta\><rsub|A*B>=5>
    and <math|<around*|(|b|)>\<Delta\><rsub|<math-bf|B>>=5,\<Delta\><rsub|A*B>=10>
  </big-figure>

  \;

  As we have seen in the previous chapter and at the beginning of this
  chapter, it is possible in our system to open a gap in two different ways:
  by breaking time-reversal symmetry (<math|\<Delta\><rsub|<math-bf|B>>\<neq\>0>)
  or by breaking inversion symmetry (<math|\<Delta\><rsub|A*B>\<neq\>0>). We
  may then wonder if the breaking of inversion symmetry also has an impact on
  the edge modes when time-reversal symmetry is broken. To investigate this,
  we display the IPR in the bulk for several different situations (see
  figures <reference|bott_5_0> <reference|dab_ipr> and <reference|ipr_18>).
  It is observed that the maximum IPR is higher (by about a factor of 3) when
  inversion symmetry is strongly broken (<math|\<Delta\><rsub|A*B>=10>). This
  means that in the presence of inversion symmetry breaking, the edge states
  tend to exist less sharply at the edges and also tend to be located within
  the bulk. This is further highlighted by figure [], which directly shows
  the IPR for a chosen disorder of W=18% and several situations where both
  symmetries are broken. This analysis should be considered in light of
  results from research on Anderson localization []. Generally, the breaking
  of time-reversal symmetry leads to a decrease in the IPR, not an increase
  as explained in [], here we observe the opposite since the maximum IPR is
  reached for <math|\<Delta\><rsub|<math-bf|B>>=5> and
  <math|\<Delta\><rsub|A*B>=10>, which indeed corresponds to a breaking of
  time-reversal symmetry. The difference here is likely related to the
  topological nature of our system. The decrease in IPR is accompanied by an
  increase in the localization length of the modes defined as:

  <\equation>
    \<xi\>=<frac|a|<sqrt|IPR>>
  </equation>

  <\big-figure|<image|figs/figipr18.pdf|0.5par|||>>
    <label|ipr_18>Sections of <reference|bott_5_0> (c) (red),
    <reference|dab_ipr> (a) (green), and <reference|dab_ipr> (b) (blue) at a
    given disorder <math|W=18>, averaged over 2000 realizations of disorder.
    Error bars show the standard error of the mean. Vertical dashed lines
    show band gap edges.
  </big-figure>

  <section|The emergence of a topological Anderson insulator>

  We now come to one of the most interesting results of this work: the
  emergence of a Topological Anderson Insulator (TAI) in our system. The
  concept of TAI was introduced in Chapter 1, but I will provide a brief
  intuitive definition here: we consider a system to exhibit a TAI when a
  topological phase emerges due to the introduction of disorder. Thus,
  disorder is responsible for the emergence of this topological phase, and it
  is not only a topological phase resistant to disorder. This behavior is
  quite surprising and atypical, arising from collective effects. As we have
  observed so far in this chapter, disorder has mostly had the effect of
  destroying the gap and the associated topological phase. Therefore, it is
  unexpected that it could play the opposite role. But it is possible.

  To achieve this, we start from a situation without a gap and introduce
  disorder in the hope of observing the emergence of a topological phase. The
  Bott index will serve as a marker; we will calculate it as we have done so
  far to identify the topological phase more or less easily. Then, we can
  confirm that it is indeed a topological gap by calculating the IPR, decay
  time, and DOS to verify the presence of edge states. It is worth noting
  that this use of the Bott index is somewhat unusual and delicate because we
  will calculate the Bott index in situations where there is no gap, and
  thus, it is not supposed to be defined. However, as we have seen so far, in
  the absence of a gap, the Bott index always seems to take the value 0.
  Therefore, it is a suitable marker.

  Of course, we will not start from a situation where
  <math|\<Delta\><rsub|<math-bf|B>>=\<Delta\><rsub|A*B>=0> because nothing
  can induce a topological phase in this situation. Instead, we will choose
  <math|\<Delta\><rsub|<math-bf|B>>=5> and also choose
  <math|\<Delta\><rsub|A*B>=5> to avoid having a gap. Then, we introduce
  disorder as we did previously, and... nothing happens.

  Indeed, it is likely that this way of introducing disorder destroys any
  structure in the system too rapidly and thus kills any chance of observing
  a topological phase. Recall that the honeycomb structure is an essential
  component for opening a topological phase in this system. Therefore, we can
  choose to only partially disturb the system by displacing only the B sites.
  By doing so, we observe that an ``island'' where CB != 0 appears for
  disorder values ranging from 7% to 35% <reference|tai_1>. It turns out that
  the edge modes are concentrated in this region, as highlighted by the
  contour plot. These localized modes appear for disorder values ranging from
  20% to 30%. This contrasts with the physics of Anderson localization, which
  suggests that localized modes exist in a certain frequency band regardless
  of the level of disorder. Despite both TAI and Anderson localization
  sharing the name ``Anderson,'' they are nevertheless two different
  situations with their own conditions of existence and properties.

  <\big-figure|<image|figs/fig55.pdf|0.9par|||>>
    <label|tai_1>Opening of a topological pseudogap by disorder. The four
    plots show the same quantities as <reference|bott_5_0> but for
    <math|\<Delta\><rsub|<math-bf|B>>=\<Delta\><rsub|A*B>=5> and disorder on
    atoms <math|B> only.
  </big-figure>

  Before delving further into the conditions for the existence of our TAI,
  let's focus on an unexpected behavior of our system. By examining figures
  <reference|bott_5_0> and <reference|tai_1>, we can observe the existence of
  edge states outside the region marked as topological by the Bott index. The
  existence of these modes can be justified by two phenomena: firstly, the
  magnetic field can indeed create edge states outside the gap region, at
  almost any frequency. However, unlike the modes within the gap, these edge
  states also have twin modes, with frequencies almost equal but located in
  the bulk. For this reason, they do not benefit from topological protection
  against disorder, as the slightest perturbation can couple them to their
  twin mode, unlike the modes within the gap which persist up to 25%
  disorder.

  Another reason for the appearance of these modes outside the bandgap is the
  corners of the hexagons. Indeed, we observe modes exclusively located on
  the corners because the armchair symmetry is broken there. We show one of
  these edge states in [TODO figure]. These edge states exist, of course, in
  the absence of disorder and resist up to a disorder of about 10%. Another
  anomaly caused by these extremely localized modes is shown in [], where for
  <math|<around*|(|\<omega\>-\<omega\><rsub|0>|)>/\<Gamma\><rsub|0>>, we
  observe a region where <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>>.
  One might then wonder if these modes are the trace of second-order topology
  [], but this would take us too far and is not addressed in this manuscript.

  <\big-figure|<image|figs/fig_bott.pdf|0.9par|||>>
    <label|tai_2>Average Bott index <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>>
    calculated for four different values of
    <math|\<Delta\><rsub|<math-bf|B>>> around
    <math|\<Delta\><rsub|<math-bf|B>>=\<Delta\><rsub|A*B>=5> for
    <math|a=\<lambda\><rsub|0>/20> in a rectangular sample shown in
    <reference|disorder_illus> The white contour shows the boundary
    <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-0.5> between the
    topologically nontrivial region where
    \ <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-1> (violet) and
    the trivial region where \ <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-0>
    (red). Ensemble averaging is performed over 60 independent realizations
    of disorder for each graph.
  </big-figure>

  We will now focus more extensively on the characterization of the TAI. This
  is to provide future experimenters with a more precise idea of where to
  look. For this purpose, we can vary the value of
  <math|\<Delta\><rsub|<math-bf|B>>> around the point
  <math|\<Delta\><rsub|<math-bf|B>>=\<Delta\><rsub|A*B>=5>, see
  <reference|tai_2>. Of course, for <math|\<Delta\><rsub|<math-bf|\<Beta\>>>\<gtr\>\<Delta\><rsub|A*B>>,
  we observe the closure of the gap due to disorder, whereas for
  \ <math|\<Delta\><rsub|<math-bf|\<Beta\>>>\<less\>\<Delta\><rsub|A*B>>, we
  have a gap that is initially trivial but becomes topological due to
  disorder. Finally, it can be observed that for a too small value of
  \ <math|\<Delta\><rsub|<math-bf|\<Beta\>>>=3>, our topological phase seems
  to disappear completely.

  As mentioned earlier, we cannot rely solely on the study of the Bott index
  to conclude on the existence of a band gap. Let's look at
  <reference|tai_ipr_dos> for the DOS and IPR in the case of
  \ <math|\<Delta\><rsub|<math-bf|\<Beta\>>>=4\<less\>\<Delta\><rsub|A*B>=5>.
  The transition between the trivial gap and the topological gap containing
  edge states is clearly visible in the DOS. The fact that these are edge
  states is highlighted by the IPR plot, while the appearance of states in a
  region devoid of states is evident in the DOS. By separately plotting the
  DOS in the bulk and at the edge, we clearly show that these states
  transition from bulk states to edge states.

  We can even go further and instead of plotting the contour
  <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-0.5> in the
  <math|<around*|(|W,\<omega\>|)>> plane, we can plot it in the
  <math|<around*|(|W,\<omega\>,\<Delta\><rsub|A*B>|)>> space for a given
  value of <math|\<Delta\><rsub|A*B>>, see <reference|phase_diagram_3d>. This
  way, we highlight that the topological phase emerging due to disorder is
  indeed connected to the topological phase arising solely from the magnetic
  field and resisting disorder. The fact that these two phases are connected
  is not surprising and has already been described for QSH insulators
  [<hlink|https://arxiv.org/abs/1102.4535|https://arxiv.org/abs/1102.4535>,
  <hlink|https://journals.aps.org/prb/abstract/10.1103/PhysRevB.87.205141<hlink||https://arxiv.org/pdf/1211.5026>|https://journals.aps.org/prb/abstract/10.1103/PhysRevB.87.205141>].

  \;

  \;

  <\big-figure|<image|figs/fig_3d.pdf|0.9par|||>>
    <label|phase_diagram_3d><math|<around*|(|a|)>> Phase diagram of the 2D
    disordered honeycomb atomic lattice in the 3D parameter space
    <math|<around*|(|W,\<omega\>,\<Delta\><rsub|<math-bf|B>>|)>> for
    <math|a=\<lambda\><rsub|0>/20> and <math|\<Delta\><rsub|A*B>=5> The blue
    surface shows the boundary between the topologically nontrivial (inside,
    <math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-1>) and trivial
    (outside,<math|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=0>)
    regions of the parameter space. The orange plane
    <math|\<Delta\><rsub|<math-bf|B>>=\<Delta\><rsub|A*B>> separates the TI
    phase for <math|\<Delta\><rsub|<math-bf|B>>\<gtr\>\<Delta\><rsub|A*B>>
    from TAI phase for <math|\<Delta\><rsub|<math-bf|B>>\<less\>\<Delta\><rsub|A*B>>.
    <math|<around*|(|b|)>> 3D density plot of average bulk IPR with the
    orange plane separating TI and TAI as in (a). For clarity, the values
    less than <math|0.006> are not shown (transparent).
  </big-figure>

  <\big-figure|<image|figs/fig_dosipr.pdf|0.9par|||>>
    <label|tai_ipr_dos>Average full <math|<around*|(|a|)>>, bulk
    <math|<around*|(|b|)>> and edge <math|<around*|(|c|)>> DOS in a
    hexagon-shaped sample shown in <reference|disorder_illus><space|1em>for
    <math|a=\<lambda\><rsub|0>/20>, <math|\<Delta\><rsub|A*B>=5> and
    <math|\<Delta\><rsub|<math-bf|B>>=4>. Average IPR of bulk modes is shown
    in <math|<around*|(|d|)>>. Ensemble averaging is performed over 200
    independent realizations of disorder for each graph. The white contour in
    <math|<around*|(|a-d|)>> shows the boundary of the topologically
    nontrivial region from <reference|tai_2> (c)
  </big-figure>

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|2level|<tuple|1|1>>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-10|<tuple|6|5>>
    <associate|auto-11|<tuple|4.1|6>>
    <associate|auto-12|<tuple|7|7>>
    <associate|auto-13|<tuple|8|7>>
    <associate|auto-14|<tuple|9|8>>
    <associate|auto-15|<tuple|5|8>>
    <associate|auto-16|<tuple|10|9>>
    <associate|auto-17|<tuple|11|10>>
    <associate|auto-18|<tuple|12|11>>
    <associate|auto-19|<tuple|13|11>>
    <associate|auto-2|<tuple|1|1>>
    <associate|auto-3|<tuple|2|2>>
    <associate|auto-4|<tuple|2|2>>
    <associate|auto-5|<tuple|3|3>>
    <associate|auto-6|<tuple|3|4>>
    <associate|auto-7|<tuple|4|4>>
    <associate|auto-8|<tuple|5|5>>
    <associate|auto-9|<tuple|4|5>>
    <associate|bott_5_0|<tuple|7|7>>
    <associate|bott_chern|<tuple|4|4>>
    <associate|bott_inv_dagger|<tuple|5|5>>
    <associate|dab_ipr|<tuple|8|7>>
    <associate|disorder_illus|<tuple|6|5>>
    <associate|dos_ipr|<tuple|3|3>>
    <associate|ipr_18|<tuple|9|8>>
    <associate|phase_diagram_3d|<tuple|12|11>>
    <associate|samples|<tuple|2|2>>
    <associate|tai_1|<tuple|10|9>>
    <associate|tai_2|<tuple|11|10>>
    <associate|tai_ipr_dos|<tuple|13|11>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Each atom is treated as a simplified two-level system with the total
        angular momentum in the ground state
        <with|mode|<quote|math>|J<rsub|g>=0> and in the excited state
        <with|mode|<quote|math>|J<rsub|e>=1>. The excited state is triply
        degenerate. The energy required for the excitation is
        <with|mode|<quote|math>|\<hbar\>\<omega\><rsub|0>> and the decay time
        is <with|mode|<quote|math>|1/\<Gamma\><rsub|0>>.
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        (Left) Honeycomb lattice of two-level atoms. Atoms
        <with|mode|<quote|math>|A> (red) and <with|mode|<quote|math>|B>
        (blue) form a unit cell. We split the finite-size lattice into a
        circular bulk region (shaded in grey) and the remaining edge regions;
        the narrowest part of the edge has a width of
        <with|mode|<quote|math>|N<rsub|edge>> atomic layers. For the purpose
        of illustration, the lattices shown are much smaller than theactual
        ones used in the calculations. Time-reversal symmetry can be broken
        by a magnetic field <with|mode|<quote|math>|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>
        perpendicular to the <with|mode|<quote|math>|x*y> plane of the
        lattice. Inversion symmetry can be broken by distinguishing between
        atoms <with|mode|<quote|math>|A> and <with|mode|<quote|math>|B>.
        Disorder is introduced by displacing atoms by random distances
        <with|mode|<quote|math>|\<delta\>r<rsub|m>\<in\><around*|[|0,W|]>> in
        random directions. (Right) Honeycomb lattice divided into rectangle
        to reveal the three types of edges: Zigzag, Armchair, and Bearded.
        The yellow wavy lines indicate that interactions occur through the
        magnetic field, indicating that it is no longer a tight-binding
        model.
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        \;
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        Comparison between the calculation of the Bott index using the
        formula with the inverses on the left and the daggers on the right.
        Average performed over 50 realizations for 120 sites with
        <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>=5>
        and <with|mode|<quote|math>|\<Delta\><rsub|A*B>=0>. For such a small
        numbers of sites, visible differences appear.
      </surround>|<pageref|auto-8>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        <with|mode|<quote|math>|<around*|(|a|)>> A rectangular lattice used
        for the calculation of Bott index. The lattice used for the
        calculations in this work consists of <with|mode|<quote|math>|N=2244>
        atoms and is as close as possible to a square.
        <with|mode|<quote|math>|<around*|(|b|)>> A lattice having the shape
        of a hexagon with armchair edges that do not support edge states in
        the absence of magnetic filed (i.e., for
        <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>=0>)
        The lattice used for the calculations in this work consists of
        <with|mode|<quote|math>|N=4326> atoms. The edge of the lattice is
        defined by <with|mode|<quote|math>|h<rsub|edge>=<around*|(|N<rsub|edge>-1|)>a<sqrt|3>/2>
        and the remaining grey part is referred to as \Pbulk\Q. We use
        <with|mode|<quote|math>|N<rsub|edge>=4> in this work.
      </surround>|<pageref|auto-10>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7>|>
        Closing of a topological band gap by disorder. (a) Average Bott index
        <with|mode|<quote|math>|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>>
        as a function of disorder strength <with|mode|<quote|math>|W> for
        \ <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>=5>
        and <with|mode|<quote|math>|\<Delta\><rsub|A*B>> = 0. The white
        contour reproduced for reference in all plots
        <with|mode|<quote|math>|<around*|(|a|)>-<around*|(|c|)>> shows the
        level <with|mode|<quote|math>|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>>
        = \<minus\>0.5. <with|mode|<quote|math>|<around*|(|b|)>> Average edge
        DOS. Dashed lines show edge modes predicted by the perturbation
        theory. Frequencies of the lowest and highest frequency edge modes
        traced up to higher values of <with|mode|<quote|math>|W> approximate
        band edges. <with|mode|<quote|math>|<around*|(|c|)>> Average bulk
        IPR. Deep violet color corresponds to either very low IPR or DOS = 0
        (no modes). <with|mode|<quote|math>|<around*|(|d|)>> Average
        quasimode decay rate in units of the natural decay rate
        <with|mode|<quote|math>|\<Gamma\><rsub|0>> of an isolated atom.
        <with|mode|<quote|math>|N<rsub|sites>=2244> averaged over
        <with|mode|<quote|math>|50-200> realizations.
      </surround>|<pageref|auto-12>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|8>|>
        Impact of topological properties on spatial localization of modes.
        Mean bulk IPR in the vicinity of topologically trivial gaps for
        <with|mode|<quote|math>|<around*|(|a|)>
        \<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>=0,\<Delta\><rsub|A*B>=5>
        and <with|mode|<quote|math>|<around*|(|b|)>\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>=5,\<Delta\><rsub|A*B>=10>
      </surround>|<pageref|auto-13>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9>|>
        Sections of <reference|bott_5_0> (c) (red), <reference|dab_ipr> (a)
        (green), and <reference|dab_ipr> (b) (blue) at a given disorder
        <with|mode|<quote|math>|W=18>, averaged over 2000 realizations of
        disorder. Error bars show the standard error of the mean. Vertical
        dashed lines show band gap edges.
      </surround>|<pageref|auto-14>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|10>|>
        Opening of a topological pseudogap by disorder. The four plots show
        the same quantities as <reference|bott_5_0> but for
        <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>=\<Delta\><rsub|A*B>=5>
        and disorder on atoms <with|mode|<quote|math>|B> only.
      </surround>|<pageref|auto-16>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11>|>
        Average Bott index <with|mode|<quote|math>|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>>
        calculated for four different values of
        <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>>
        around <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>=\<Delta\><rsub|A*B>=5>
        for <with|mode|<quote|math>|a=\<lambda\><rsub|0>/20> in a rectangular
        sample shown in <reference|disorder_illus> The white contour shows
        the boundary <with|mode|<quote|math>|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-0.5>
        between the topologically nontrivial region where
        \ <with|mode|<quote|math>|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-1>
        (violet) and the trivial region where
        \ <with|mode|<quote|math>|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-0>
        (red). Ensemble averaging is performed over 60 independent
        realizations of disorder for each graph.
      </surround>|<pageref|auto-17>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|12>|>
        <with|mode|<quote|math>|<around*|(|a|)>> Phase diagram of the 2D
        disordered honeycomb atomic lattice in the 3D parameter space
        <with|mode|<quote|math>|<around*|(|W,\<omega\>,\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>|)>>
        for <with|mode|<quote|math>|a=\<lambda\><rsub|0>/20> and
        <with|mode|<quote|math>|\<Delta\><rsub|A*B>=5> The blue surface shows
        the boundary between the topologically nontrivial (inside,
        <with|mode|<quote|math>|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=-1>)
        and trivial (outside,<with|mode|<quote|math>|<around*|\<langle\>|C<rsub|\<Beta\>>|\<rangle\>>=0>)
        regions of the parameter space. The orange plane
        <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>=\<Delta\><rsub|A*B>>
        separates the TI phase for <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>\<gtr\>\<Delta\><rsub|A*B>>
        from TAI phase for <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>\<less\>\<Delta\><rsub|A*B>>.
        <with|mode|<quote|math>|<around*|(|b|)>> 3D density plot of average
        bulk IPR with the orange plane separating TI and TAI as in (a). For
        clarity, the values less than <with|mode|<quote|math>|0.006> are not
        shown (transparent).
      </surround>|<pageref|auto-18>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13>|>
        Average full <with|mode|<quote|math>|<around*|(|a|)>>, bulk
        <with|mode|<quote|math>|<around*|(|b|)>> and edge
        <with|mode|<quote|math>|<around*|(|c|)>> DOS in a hexagon-shaped
        sample shown in <reference|disorder_illus><space|1em>for
        <with|mode|<quote|math>|a=\<lambda\><rsub|0>/20>,
        <with|mode|<quote|math>|\<Delta\><rsub|A*B>=5> and
        <with|mode|<quote|math>|\<Delta\><rsub|<rigid|<with|mode|<quote|text>|<with|font-family|<quote|rm>|font-series|<quote|bold>|font-shape|<quote|right>|B>>>>=4>.
        Average IPR of bulk modes is shown in
        <with|mode|<quote|math>|<around*|(|d|)>>. Ensemble averaging is
        performed over 200 independent realizations of disorder for each
        graph. The white contour in <with|mode|<quote|math>|<around*|(|a-d|)>>
        shows the boundary of the topologically nontrivial region from
        <reference|tai_2> (c)
      </surround>|<pageref|auto-19>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Our
      system: A two-dimensional honeycomb atomic lattice>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Density
      of states and band gaps> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Computing
      the Bott index: a topological marker>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Introducing
      disorder > <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9><vspace|0.5fn>

      <with|par-left|<quote|1tab>|4.1<space|2spc>Disorder as a topological
      killer <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|5<space|2spc>The
      emergence of a topological Anderson insulator>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>