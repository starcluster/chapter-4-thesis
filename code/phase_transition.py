import time
import sys

import numpy as np
import matplotlib.pyplot as plt

import Gd
import bott_legacy

sys.path.append('../../chapter-2-thesis/code')
from utils import Color,timer
import haldane_real as hr
import lattice_r as lr
import tex
import bott


tex.useTex()

def introduce_disorder(lattice,r):
    for i in range(lattice.shape[0]):
        lattice[i][0] =  lattice[i][0] + np.random.uniform(-r,r)
        lattice[i][1] =  lattice[i][1] + np.random.uniform(-r,r)
    return lattice

def introduce_disorder_b(lattice,r):
    for i in range(lattice.shape[0]):
        if i%2 == 1:
            lattice[i][0] =  lattice[i][0] + np.random.uniform(-r,r)
            lattice[i][1] =  lattice[i][1] + np.random.uniform(-r,r)
    return lattice

def value_to_actual_closest(x,array):
    differences = np.abs(array - x)
    closest_index = np.argmin(differences)
    closest_value = array[closest_index]
    return closest_value

def plot_disorder_closing_gap():
    N_lattice = 120
    a = 1

    Δ_B = 12
    Δ_AB = 0

    lattice = lr.generate_hex_grid_lb_center(N_lattice, a)
    N_sites = lattice.shape[0]

    r_res = 10
    r_min = 0
    r_max = 0.3
    r_array = np.linspace(r_min,r_max,r_res)

    N_batch = 1


    omega_res = 10
    omega_min = -10
    omega_max = 30
    omega_array = np.linspace(omega_max,omega_min,omega_res)

    bott_map = np.zeros((r_res,2*N_sites))
    bott_array_over_batch = np.zeros((r_res, omega_res))
    bott_array = np.zeros((r_res, omega_res))
    
    for batch in range(N_batch):
        print(batch)
        bott_array = np.zeros((r_res, omega_res))
        for i,r in enumerate(r_array):
            lattice = lr.generate_hex_grid_lb_center(N_lattice, a)
            lattice = introduce_disorder(lattice,r)
            M = Gd.create_matrix_TE(lattice, Δ_B, Δ_AB, 0.05 * 2 * np.pi)

            N = M.shape[0]
            
            w, v = np.linalg.eig(M)
                
            idx = w.argsort()[::-1]
            w = w[idx]
            v = v[:, idx]
            ω = 7

            f, v = bott.compute_frequencies(w, v)
            
            b = bott.all_bott(lattice, v, f)

            for k,omega in enumerate(omega_array):
                bott_array[i,k] = b[value_to_actual_closest(omega, f)]
            #     print(value_to_actual_closest(omega, f))
            #     print(b[value_to_actual_closest(omega, f)])
            # exit()
            print(np.sum(bott_array_over_batch))
            print(np.sum(bott_array))
            # exit()
            for k,x in enumerate(b):
                bott_map[i,k] += x
        bott_array_over_batch += bott_array
        
    print(bott_array_over_batch)
    # plt.imshow(bott_array_over_batch.T/N_batch)
    # plt.colorbar()
    # plt.show()

    # data = np.random.rand(10, 10)  # Remplacez cela par vos données réelles
    # x_values = np.linspace(0.1, 1.0, data.shape[1])  # Exemple pour l'axe x
    # y_values = np.linspace(0.2, 2.0, data.shape[0])  # Exemple pour l'axe y
    cmap = plt.get_cmap("viridis")  # Remplacez "viridis" par le nom de la colormap souhaitée
    fig, ax = plt.subplots()
    im = ax.imshow(bott_array_over_batch.T/N_batch, cmap=cmap, extent=[r_min*100, r_max*100, omega_min, omega_max],aspect='auto')    
    cbar = fig.colorbar(im)
    ax.set_xlabel('Axe X')
    ax.set_ylabel('Axe Y')
    plt.show()

        
if __name__ == "__main__":
    N_lattice = 120
    a = 1

    Δ_B = 12
    Δ_AB = 0
    k0 = 2*0.05*np.pi/a

    ass = np.linspace(1,3,20)

    for a in ass:
        lattice = lr.generate_hex_grid_lb_center(N_lattice, a)
        k0 = 2*0.05*np.pi
    
    
        M = Gd.create_matrix_TE(lattice, Δ_B, Δ_AB,k0)

        N = M.shape[0]
            
        w, v = np.linalg.eig(M)
                
        idx = w.argsort()[::-1]
        w = w[idx]
        v = v[:, idx]
        ω = 7

        f, v = bott.compute_frequencies(w, v)
    
        b = bott.bott(lattice, v, f, ω, pol=True,dagger=True)

        # (lattice, eigvec, frequencies, omega, pol=False, dagger=False, projector=False, verbose=False, vl=None)
        print(f"{a=},{b=}")

        bs = bott.all_bott(lattice=lattice, eigvec=v, frequencies=f, pol=True)
        # bs = bott_legacy.all_bott(lattice=lattice, eigvec=v, frequencies=f)


        # print(len(f))
        # print(len(bs))

        plt.hist(f, bins=80, histtype="step",color="blue",label="")
        plt.scatter(bs.keys(), bs.values(), color="red")
        plt.xlabel(r"$\omega$",fontsize=20)
        plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
        # plt.title(f"$a={np.round(a,3)}$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.savefig(f"a={np.round(a,3)}.png",format="png",bbox_inches='tight')

        plt.clf()
        plt.cla()

        
        
