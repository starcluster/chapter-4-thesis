import time
import sys

import numpy as np

import Gd
import bott

sys.path.append('../../chapter-2-thesis/code')
from utils import Color,timer
import haldane_real as hr
import lattice_r as lr
import tex

tex.useTex()

def bott_th(Δ_B, Δ_AB):
    if np.abs(Δ_B) >= np.abs(Δ_AB):
        return 1
    else:
        return 0

if __name__ == "__main__":
    print(
        "Python {:s} {:03d}bit on {:s}\n".format(
            " ".join(item.strip() for item in sys.version.split("\n")),
            64 if sys.maxsize > 0x100000000 else 32,
            sys.platform,
        )
    )
    print(f"NumPy: {np.version.version}\n")

    # HALDANE
    a = 1
    t1 = 1
    t2 = 0.2j
    M = 0.1
    N_cluster_side = 6

    # easy optimization generate one template for t2 t1 and generate H_haldane fast
    lattice = lr.generate_hex_grid_stretch(N_cluster_side=N_cluster_side,a=a)
    for M in [0.7+i/20 for i in range(20)]:
        H = hr.create_hamiltonian_haldane_pbc(lattice, a, t1, t2, M)
        eigenvalues, eigenvectors = np.linalg.eig(H)
        els, evs, _ = hr.sort_vectors(eigenvalues, eigenvectors, eigenvectors)
        print(M,",",bott.bott(lattice=lattice, eigvec=evs, frequencies=els, omega=0, pol=False, dagger=False, verbose=False))
    exit()
    # POLARIZED

    
    N = 60
    a = 1
    s = 1.00

    grid = lr.generate_hex_grid_lb_center(N, a)

    Δ_B_array = np.linspace(-12,12,24)
    Δ_AB_array = np.linspace(-12,12,24)

    times_dagger = []
    times_inv = []
    for Δ_B in Δ_B_array: 
        for Δ_AB in Δ_AB_array:
            if np.abs(np.abs(Δ_B)-np.abs(Δ_AB)) > 0.1:
                M = Gd.create_matrix_TE(grid, Δ_B, Δ_AB, 0.05 * 2 * np.pi)

                N = M.shape[0]

                w, v = np.linalg.eig(M)
                
                idx = w.argsort()[::-1]
                w = w[idx]
                v = v[:, idx]
                ω = 7

                f, v = bott.compute_frequencies(w, v)

                t0 = time.time()
                b = bott.bott(grid, v, f, ω, pol=True, dagger=True, verbose=False)
                t1 = time.time()
                times_dagger.append(t1-t0)
                
                b_th = bott_th(Δ_B,Δ_AB)
                print(f"{Δ_B},{Δ_AB},{b},{b_th}") 
                if np.abs(np.abs(b)-b_th)<1e-6:
                    print(f"{Color['GREEN']}Success ! {Color['END']}")
                else:
                    print(f"{Color['RED']}Fail ! {Color['END']}")
                    # exit()

                # print(f"Time Bott python: {t1-t0}")
        
    print(np.mean(times_dagger))
    print(np.std(times_dagger))

    # print(np.mean(times_inv))
    # print(np.std(times_inv))
