import numpy as np
import matplotlib.pyplot as plt
import random
import ctypes
import time as t
import ll2D
import pandas as pd

from numpy.linalg import eig, eigh

def create_matrix_TM(lattice, delta_B, delta_AB, k0):
    # k0 = 2*np.pi*0.09
    N = lattice.shape[0]
    M = np.zeros((N, N), dtype=complex)
    for i in range(N):
        for j in range(N):
            if i == j:
                if i < int(N / 2):
                    delta_AB_eff = -delta_AB
                else:
                    delta_AB_eff = delta_AB
                M[i, j] = 1j
            else:
                x = (lattice[i][0] - lattice[j][0]) * k0
                y = (lattice[i][1] - lattice[j][1]) * k0
                M[i, j] = libc.gzz_no_plates_re(x, y) + 1j * libc.gzz_no_plates_im(x, y)
    return (-3 / 2) * M


def createMatrixVectorSum(lattice, d, delta_B, delta_AB, k0):
    N = lattice.shape[0]
    M = np.zeros((2 * N, 2 * N), dtype=complex)
    for i in range(N):
        for j in range(N):
            if i == j:
                sgn = 1
                if i < N / 2:
                    sgn = 1
                else:
                    sgn = -1
                if d < np.pi:
                    M[2 * i, 2 * j] = 2 * delta_B + 2 * delta_AB * sgn
                    M[2 * i + 1, 2 * j + 1] = -2 * delta_B + 2 * delta_AB * sgn
                else:
                    M[2 * i, 2 * j] = (
                        -1j * (libc.sum_diag_im(d) * 2 + 1)
                        + 2 * delta_B
                        + 2 * delta_AB * sgn
                    )
                    # why + 1 ? gives the correct result but does't agree with equation
                    M[2 * i + 1, 2 * j + 1] = (
                        -1j * (libc.sum_diag_im(d) * 2 + 1)
                        - 2 * delta_B
                        + 2 * delta_AB * sgn
                    )

            else:
                x = (lattice[i][0] - lattice[j][0]) * k0
                y = (lattice[i][1] - lattice[j][1]) * k0
                imag_sum = int(not (d < np.pi))
                dk0 = d * k0
                M[2 * i, 2 * j] = (3 / 2) * (
                    libc.sum_gxx_re(x, y, dk0)
                    + 1j * libc.sum_gxx_im(x, y, dk0) * imag_sum
                )
                M[2 * i + 1, 2 * j + 1] = M[2 * i, 2 * j]
                M[2 * i, 2 * j + 1] = (3 / 2) * (
                    libc.sum_gxy_re(x, y, dk0)
                    + 1j * libc.sum_gxy_im(x, y, dk0) * imag_sum
                )
                M[2 * i + 1, 2 * j] = (3 / 2) * (
                    libc.sum_gyx_re(x, y, dk0)
                    + 1j * libc.sum_gyx_im(x, y, dk0) * imag_sum
                )

    return M


def create_matrix_TE(lattice, delta_B, delta_AB, k0):
    N = lattice.shape[0]
    M = np.zeros((2 * N, 2 * N), dtype=complex)

    for i in range(N):
        for j in range(N):
            if i == j:
                sgn = 1
                if i < N / 2:
                    sgn = 1
                else:
                    sgn = -1
                M[2 * i, 2 * j] = 1j + 2 * delta_B + 2 * sgn * delta_AB
                M[2 * i + 1, 2 * j + 1] = 1j - 2 * delta_B + 2 * sgn * delta_AB

            else:
                x = (lattice[i][0] - lattice[j][0]) * k0
                y = (lattice[i][1] - lattice[j][1]) * k0
                r = np.sqrt(x * x + y * y)
                tmp1 = 3.0 / 2.0 * np.exp(1j * r) / r
                tmp2 = ll2D.P(1j * r)
                tmp3 = ll2D.Q(1j * r)
                M[2 * i, 2 * j] = tmp1 * (tmp2 + tmp3 * x * x / (r * r))
                M[2 * i, 2 * j + 1] = tmp1 * (tmp3 * x * y / (r * r))
                M[2 * i + 1, 2 * j] = tmp1 * (tmp3 * x * y / (r * r))
                M[2 * i + 1, 2 * j + 1] = tmp1 * (tmp2 + tmp3 * y * y / (r * r))

    sr2 = 1 / np.sqrt(2)
    deg = np.array([[sr2, 1j * sr2], [-sr2, 1j * sr2]])
    for i in range(N):
        for j in range(N):
            if i != j:
                block = np.copy(M[2 * i : 2 * i + 2, 2 * j : 2 * j + 2])
                block2 = np.dot(np.dot(deg, block), np.conj(deg).T)
                M[2 * i : 2 * i + 2, 2 * j : 2 * j + 2] = np.copy(block2)
    return M


def create_matrix_TE_plates(lattice, delta_B, delta_AB, k0, d):
    N = lattice.shape[0]
    M = np.zeros((2 * N, 2 * N), dtype=complex)
    K = 400
    for i in range(N):
        for j in range(N):
            if i == j:
                sgn = 1
                if i < N / 2:
                    sgn = 1
                else:
                    sgn = -1
                if d < np.pi:
                    M[2 * i, 2 * j] = 2 * delta_B + 2 * delta_AB * sgn
                    M[2 * i + 1, 2 * j + 1] = -2 * delta_B + 2 * delta_AB * sgn
                else:
                    M[2 * i, 2 * j] = (
                        -1j * (libc.sum_diag_im(d) * 2 + 1)
                        + 2 * delta_B
                        + 2 * delta_AB * sgn
                    )
                    # why + 1 ? gives the correct result but does't agree with equation
                    M[2 * i + 1, 2 * j + 1] = (
                        -1j * (libc.sum_diag_im(d) * 2 + 1)
                        - 2 * delta_B
                        + 2 * delta_AB * sgn
                    )
            else:
                x = (lattice[i][0] - lattice[j][0]) * k0
                y = (lattice[i][1] - lattice[j][1]) * k0
                k = np.arange(-K, K + 1)
                t = k * d * k0
                rk = np.sqrt(x**2 + y**2 + t**2)
                tmp1k = 3.0 / 2.0 * np.exp(1j * rk) / rk * (-1) ** np.abs(k)
                tmp2k = ll2D.P(1j * rk)
                tmp3k = ll2D.Q(1j * rk)
                gxxk = tmp1k * (tmp2k + tmp3k * x * x / (rk * rk))
                gxyk = tmp1k * (tmp3k * x * y / (rk * rk))
                gyyk = tmp1k * (tmp2k + tmp3k * y * y / (rk * rk))
                # print(gxxk,gxyk,gyyk)
                # exit()
                gxx = np.sum(gxxk)
                gxy = np.sum(gxyk)
                gyy = np.sum(gyyk)
                M[2 * i, 2 * j] = gxx
                M[2 * i, 2 * j + 1] = gxy
                M[2 * i + 1, 2 * j] = gxy
                M[2 * i + 1, 2 * j + 1] = gyy

    sr2 = 1 / np.sqrt(2)
    deg = np.array([[sr2, 1j * sr2], [-sr2, 1j * sr2]])
    for i in range(N):
        for j in range(N):
            if i != j:
                block = np.copy(M[2 * i : 2 * i + 2, 2 * j : 2 * j + 2])
                block2 = np.dot(np.dot(deg, block), np.conj(deg).T)
                M[2 * i : 2 * i + 2, 2 * j : 2 * j + 2] = np.copy(block2)
    return M


def modifyMatrixVectorSum(M, d, delta_B, delta_AB):
    N = int(M.shape[0] / 2)
    for i in range(N):
        if i < int(N / 2):
            delta_AB = -delta_AB
        if d < np.pi:
            M[2 * i, 2 * i] = 2 * delta_B + 2 * delta_AB
            M[2 * i + 1, 2 * i + 1] = -2 * delta_B + 2 * delta_AB
        else:
            M[2 * i, 2 * i] = (
                -1j * (libc.sum_diag_im(d) * 2 + 1) + 2 * delta_B + 2 * delta_AB
            )
            # why + 1 ? gives the correct result but does't agree with equation
            M[2 * i + 1, 2 * i + 1] = (
                -1j * (libc.sum_diag_im(d) * 2 + 1) - 2 * delta_B + 2 * delta_AB
            )

    return M


def print_matrix(M):
    for i in M:
        print("[", end="")
        for j in i:
            print(np.round(j, 2), end=",")
        print("],")


def simulate(N):
    rho = 1
    R = np.sqrt(N / np.pi / rho)

    disc = ll2D.createDisc(R, N)

    d = 1
    for d in [1, 2, 3, 3.1, 3.14, 3.16, 3.2, 4, 5, 6, 7, 8, 9, 10]:
        t_gen0 = t.time()
        M = createMatrixVectorSum(disc, d, 0, 0)
        t_gen1 = t.time()
        print("génerer la matrice = ", t_gen1 - t_gen0)

        w, v = eig(M)
        N = M.shape[0]

        omega = [0] * N
        gamma = [0] * N
        iprs = [0] * N

        for i in range(N):
            omega[i] = -w[i].real
            gamma[i] = -w[i].imag
            iprs[i] = ll2D.IPRVector(i, v)

        f = open("vector_sum.dat", "w")
        f.write("x,y,ipr\n")
        for i in range(N):
            f.write(str(gamma[i]) + "," + str(omega[i]) + "," + str(iprs[i]) + "\n")
        f.close()
        plt.scatter(omega, gamma, c=iprs, cmap="jet", marker=".", vmin=0, vmax=0.5)
        plt.colorbar()
        # plt.yscale('log')
        plt.xlabel(r"$\omega_n$")
        plt.ylabel(r"$\gamma_n$")
        plt.axis((-10, 10, -1, 1e0))
        plt.title("$d=$" + str(d))
        plt.savefig("Gd" + str(d) + "rho" + str(rho) + ".png")
        plt.clf()
        # plt.show()
        # plt.hist(gammaS, bins=100,density=True, range=(-20,20))
        # plt.show()


if __name__ == "__main__":
    M = create_matrix_TE_plates(lattice, delta_B, delta_AB, k0, d)
    simulate(100)
