import time
import sys
import csv

import numpy as np
import matplotlib.pyplot as plt

import Gd
import ipr_pol

sys.path.append('../../chapter-2-thesis/code')
import bott
from utils import Color,timer
import haldane_real as hr
import lattice_r as lr
import tex


tex.useTex()

def introduce_disorder(lattice,r):
    for i in range(lattice.shape[0]):
        lattice[i][0] =  lattice[i][0] + np.random.uniform(-r,r)
        lattice[i][1] =  lattice[i][1] + np.random.uniform(-r,r)
    return lattice

def introduce_disorder_b(lattice,r):
    for i in range(lattice.shape[0]):
        if i%2 == 1:
            lattice[i][0] =  lattice[i][0] + np.random.uniform(-r,r)
            lattice[i][1] =  lattice[i][1] + np.random.uniform(-r,r)
    return lattice

def value_to_actual_closest(x,array):
    differences = np.abs(array - x)
    closest_index = np.argmin(differences)
    closest_value = array[closest_index]
    return closest_value

def map_disorder(Δ_B=5, Δ_AB=0, N_lattice=120, a=1, distance=0, k0=0.05 * 2 * np.pi):
    lattice = lr.generate_hex_grid_lb_center(N_lattice, a)
    # lr.display_lattice(lattice)
    N_sites = lattice.shape[0]

    r_res = 50
    r_min = 0
    r_max = 0.3
    r_array = np.linspace(r_min,r_max,r_res)

    N_batch = 10

    omega_res = 50
    omega_min = -2
    omega_max = 16
    omega_array = np.linspace(omega_min,omega_max,omega_res)

    bott_array_over_batch = np.zeros((r_res, omega_res))
    bott_array = np.zeros((r_res, omega_res))

    gamma_array_over_batch = np.zeros((r_res, omega_res))
    gamma_array = np.zeros((r_res, omega_res))


    dos_array_over_batch = np.zeros((r_res, omega_res))
    dos_array = np.zeros((r_res, omega_res))

    ipr_array_over_batch = np.zeros((r_res, omega_res))
    ipr_array = np.zeros((r_res, omega_res))

    


            
    for batch in range(N_batch):
        print(f"{batch}/{N_batch}")
        bott_array = np.zeros((r_res, omega_res))
        gamma_array = np.zeros((r_res, omega_res))
        dos_array = np.zeros((r_res, omega_res))
        ipr_array = np.zeros((r_res, omega_res))
        for i,r in enumerate(r_array):
            print(f"{i}/{r_res}")
            lattice = lr.generate_hex_grid_lb_center(N_lattice, a)
            lattice = introduce_disorder(lattice,r)
            M = Gd.create_matrix_TE(lattice, Δ_B, Δ_AB, k0)
            print("Matrix created")

            N = M.shape[0]
            print(M.shape)
            
            w, v = np.linalg.eig(M)

            print("EV found")
                
            idx = w.argsort()[::-1]
            w = w[idx]
            v = v[:, idx]
            ω = 7

            # Γs = np.imag(w)

            f, v = bott.compute_frequencies(w, v)


            # b = bott.all_bott(lattice, v, f, pol=True, dagger=True, projector=False, verbose=False, vl=None, stop=N//2+N//8)
            print("Bott index computed.")

            # for ii in range(len(f)):
            #     print(f"{f[ii]},{np.imag(w[ii])}")
            # exit()

            for k,omega in enumerate(omega_array):
                # bott_array[i,k] = b[value_to_actual_closest(omega, f)]
                bott_array[i,k] =  bott.bott(lattice, v, f, omega, pol=True, dagger=True, projector=False, verbose=False, vl=None)
                
                dω = 0.5
                dos = np.sum((f < (omega+dω)) & (f > (omega-dω)))
                # print(dos)
                # exit()
                dos_array[i,k] = dos
                if dos == 0:
                    bulk = 0
                    gamma_array[i,k] = 0
                else:
                    om = value_to_actual_closest(omega, f)
                    ind_om = np.where(f == om)
                    pos_vect = ind_om[0][0]
                    gamma_array[i,k] = np.imag(w[pos_vect]) #/np.max(np.imag(w))

                    bulk = ipr_pol.ipr_bulk(v, lattice, pos_vect, 0.3*a*int(np.sqrt(N_lattice)))
                ipr_array[i,k] = bulk
                print(f"{k=},{omega=},{bulk=},gamma={gamma_array[i,k]}")
                
                # print(value_to_actual_closest(omega, f),b[value_to_actual_closest(omega, f)])
            

        bott_array_over_batch += bott_array
        gamma_array_over_batch += gamma_array
        dos_array_over_batch += dos_array
        ipr_array_over_batch += ipr_array

        np.savetxt(f'../data/bott_{batch=}.csv', bott_array, delimiter=',',fmt='%d')

        
    bott_array_over_batch /= N_batch
    gamma_array_over_batch /= N_batch
    dos_array_over_batch /= N_batch
    ipr_array_over_batch /= N_batch

    # print(bott_array_over_batch)

    r_list,omega_list,bss,gammas,doss,iprs = [],[],[],[],[],[]
    for i,r in enumerate(r_array):
        for k,omega in enumerate(omega_array):
            r_list.append(r)
            omega_list.append(omega)
            bss.append(bott_array_over_batch[i,k])
            gammas.append(gamma_array_over_batch[i,k])
            doss.append(dos_array_over_batch[i,k])
            iprs.append(ipr_array_over_batch[i,k])

    # print(r_list)
    # print(omega_list)


    with open(f"../data/map_disorder_bott_{distance}.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)
        for k,l,m in zip(r_list,omega_list,bss):
            writer.writerow([k,l,m])

    with open(f"../data/map_disorder_gamma_{distance}.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)
        for k,l,m in zip(r_list,omega_list,gammas):
            writer.writerow([k,l,m])

    with open(f"../data/map_disorder_dos_{distance}.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)
        for k,l,m in zip(r_list,omega_list,doss):
            writer.writerow([k,l,m])

    with open(f"../data/map_disorder_ipr_{distance}.csv", mode="w", newline="") as csvf:
        writer = csv.writer(csvf)
        for k,l,m in zip(r_list,omega_list,iprs):
            writer.writerow([k,l,m])

def plot_map(name, x_label="$x$", y_label="$y$", title="",pos_title=0,cb=(-100,100)):
    data = np.genfromtxt(f"../data/{name}.csv", delimiter=',', encoding=None)

    x = np.real(data[:, 0])
    y = np.real(data[:, 1])
    z = np.real(data[:, 2])

    x_unique = np.sort(np.unique(x))
    y_unique = np.sort(np.unique(y))
    X, Y = np.meshgrid(x_unique, y_unique)

    Z = np.zeros_like(X)
    for i in range(len(x)):
        row, col = np.where((X == x[i]) & (Y == y[i]))
        Z[row, col] = z[i]

    plt.pcolormesh(X, Y, Z, shading='auto', cmap="viridis",vmin=cb[0],vmax=cb[1])
    cb = plt.colorbar()
    cb.ax.tick_params(labelsize=20)
    plt.xlabel(x_label,fontsize=20)
    plt.ylabel(y_label,fontsize=20)
    # plt.title(title,fontsize=20)
    plt.text(s=title, x=pos_title, y=15, color='white',fontsize=20)
    plt.xticks(fontsize=20,rotation=45)
    plt.yticks(fontsize=20)
    plt.savefig(f"../figs/{name}.pdf",format="pdf",bbox_inches='tight')
    plt.clf()
    plt.cla()
        
if __name__ == "__main__":
    
    distance = 0
    map_disorder(Δ_B=5, Δ_AB=0, N_lattice=120, a=1, distance=0, k0=0.05 * 2 * np.pi)
    # exit()

    plot_map(f"map_disorder_bott_{distance}",x_label=r"$\textrm{Disorder strength }W$", y_label=r"$\textrm{Frequency }(\omega - \omega_0)/\Gamma_0$",title=r"$\textrm{Bott} \langle C_{\mathrm{B}} \rangle $",pos_title=0.22,cb=(-1,0))

    plot_map(f"map_disorder_gamma_{distance}",x_label=r"$\textrm{Disorder strength }W$", y_label=r"$\textrm{Frequency }(\omega - \omega_0)/\Gamma_0$",title=r"$\textrm{Decay rate} \langle \Gamma/\Gamma_0 \rangle $",pos_title=0.15,cb=(0,1))

    plot_map(f"map_disorder_dos_{distance}",x_label=r"$\textrm{Disorder strength }W$", y_label=r"$\textrm{Frequency }(\omega - \omega_0)/\Gamma_0$",title=r"$\textrm{Edge} \langle \textrm{DOS} \rangle $",pos_title=0.23,cb=(0,3))

    plot_map(f"map_disorder_ipr_{distance}",x_label=r"$\textrm{Disorder strength }W$", y_label=r"$\textrm{Frequency }(\omega - \omega_0)/\Gamma_0$",title=r"$\textrm{Bulk } \langle \textrm{IPR} \rangle $",pos_title=0.2,cb=(0,1))
    
        
