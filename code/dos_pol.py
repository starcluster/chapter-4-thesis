import time
import sys

import numpy as np
import matplotlib.pyplot as plt

import Gd
import bott

sys.path.append('../../chapter-2-thesis/code')
from utils import Color,timer
import lattice_r as lr
import tex
import dos

tex.useTex()

def gen_hist(lattice, Δ_B, Δ_AB, k0, color,label):
    M = Gd.create_matrix_TE(lattice, Δ_B, Δ_AB, k0)
    w, v = np.linalg.eig(M)
    f = -np.real(w)/2

    plt.hist(f, bins=80, histtype="step",color=color,label=label)

def plot_dos(N_lattice = 120,a = 1,k0=2*np.pi*0.05):    
    lattice = lr.generate_hex_hex_stretch(N_lattice)
    N_sites = lattice.shape[0]

    gen_hist(lattice, 12, 0, k0, "blue",label=r"$\Delta_{\mathbf{B}} = 12 \quad \Delta_{AB} = 0$")
    gen_hist(lattice, 0, 12, k0,"red",label=r"$\Delta_{\mathbf{B}} = 0 \quad \Delta_{AB} = 12$")
    legend = plt.legend(fontsize=20)


    plt.xlabel(r"$\textrm{Frequency } (\omega-\omega_0)/\Gamma_0$",fontsize=20)
    plt.ylabel(r"$\textrm{DOS}$",fontsize=20)
    plt.title(f"$N={N_sites}$",fontsize=20)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.savefig(f"../figs/dos.pdf",format="pdf",bbox_inches='tight')
    legend.remove()
    plt.axis((-10,25,0,17))
    plt.savefig(f"../figs/dos_zoom.pdf",format="pdf",bbox_inches='tight')
    plt.show()



if __name__ == "__main__":
    N_lattice = 600
    a = 1
    k0 = 2*0.05*np.pi

    plot_dos(N_lattice = N_lattice,a = a,k0=k0)
        
        
