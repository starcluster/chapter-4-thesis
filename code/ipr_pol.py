import time
import sys

import numpy as np
import matplotlib.pyplot as plt

import Gd

sys.path.append('../../chapter-2-thesis/code')
from utils import Color,timer
import haldane_real as hr
import lattice_r as lr
import tex

tex.useTex()

def intensity_map(v, grid, m, omega):
    N = v.shape[0]
    intensities = []
    x, y = [], []
    for i in range(0, N, 2):
        intensities.append(np.abs(v[i][m]) ** 2 + np.abs(v[i + 1][m]) ** 2)
        x.append(grid[int(i / 2)][0])
        y.append(grid[int(i / 2)][1])


    sizes = [x * 10000 for x in intensities]
    fig, ax  = plt.subplots()

    im = ax.scatter(x, y, c=intensities, s=sizes, cmap="plasma")
    ax.set_xlabel(r"$x$",fontsize=20)
    ax.set_ylabel(r"$y$",fontsize=20)
    # ax.set_xticks([],fontsize=20)
    # ax.set_yticks([],fontsize=20)
    ax.tick_params(labelsize=20)
    ax.set_title(r"$\omega \approx" + f"{np.round(omega,1)}$",fontsize=20)

    cbar_ax = fig.add_axes([0.93, 0.11, 0.05, 0.71])
    clb = fig.colorbar(im, cax=cbar_ax)
    clb.ax.tick_params(labelsize=20)
    clb.ax.set_title(r"$\quad\;\; |\psi_i^m|^2$",fontsize=20)
    plt.savefig(f"../figs/imaps/imap_{np.round(omega,1)}.pdf",format="pdf",bbox_inches='tight')
    plt.savefig(f"../figs/imaps/imap_{np.round(omega,1)}.png",format="png",bbox_inches='tight')
    plt.clf()
    plt.cla()
    # plt.show()

def ipr_bulk(vect, grid, mode, radius):
    n_sites = vect.shape[0]
    intensities = []
    x,y = grid.T
    x,y = x.tolist(), y.tolist()
    x.sort()
    y.sort()
    x_middle = x[n_sites//4]
    y_middle = y[n_sites//4]

    x_circle,y_circle = [],[]
    for i in range(0, n_sites, 2):
        x_site = grid[i // 2][0]
        y_site = grid[i // 2][1]
        if (x_site-x_middle)**2 + (y_site-y_middle)**2 < radius**2:
            intensities.append(np.abs(vect[i][mode]) ** 2 + np.abs(vect[i + 1][mode]) ** 2)
            x_circle.append(x_site)
            y_circle.append(y_site)

    # plt.scatter(x_circle,y_circle,c=intensities)
    # plt.colorbar()
    # plt.show()
    # plt.savefig("../figs/check_ipr.pdf",format="pdf",bbox_inches='tight')

    # plt.show()
            
    return np.sum(intensities)

if __name__ == "__main__":
    N_lattice = 200
    a = 1
    k0 = 2*0.05*np.pi

    Δ_B = 0
    Δ_AB = 12

    lattice = lr.generate_hex_hex_stretch(N_lattice)
    
    M = Gd.create_matrix_TE(lattice, Δ_B, Δ_AB, k0)
    w, v = np.linalg.eig(M)

    f = -np.real(w)/2

    
    for i,x in enumerate(f):
        if np.abs(x) < 20:
            print(i,x)
    intensity_map(v, lattice, 188, f[188])
    
        
        
