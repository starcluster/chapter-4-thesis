import time
import sys
import csv

import numpy as np
import matplotlib.pyplot as plt

import Gd


sys.path.append('../../chapter-2-thesis/code')
import bott
from utils import Color,timer
import haldane_real as hr
import lattice_r as lr
import tex
import study_uvuv as suv


tex.useTex()
        
if __name__ == "__main__":
    plt.style.use('dark_background')
    N_lattice = 200
    a = 1
    lattice = lr.generate_hex_grid_lb_center(N_lattice, a)
    N_sites = lattice.shape[0]

    Δ_B = 5
    Δ_AB = 0
    k0 = 2*0.072*np.pi

    M = Gd.create_matrix_TE(lattice, Δ_B, Δ_AB, k0)
    N = M.shape[0]
    w, v = np.linalg.eig(M)
            
    idx = w.argsort()[::-1]
    w = w[idx]
    v = v[:, idx]
    ω = 7
    
    f, v = bott.compute_frequencies(w, v)
    kp = np.searchsorted(f, 7)
    U,V = bott.compute_vxvy_w(lattice, v, kp)
        
    uvuv = U@V@np.linalg.inv(U)@np.linalg.inv(V)
    λ_uvuv, ψ_uvuv = np.linalg.eig(uvuv)

    current_points = np.array(list(zip(np.real(λ_uvuv),np.imag(λ_uvuv))))

    plt.axhline(y=0,color="white",zorder=0,lw=0.3)
    plt.axvline(x=0,color="white",zorder=0,lw=0.3)

    k0s2pis = np.linspace(0.072,0.073,100)

    plt.axhline(y=0,color="white",zorder=0,lw=0.3)
    plt.axvline(x=0,color="white",zorder=0,lw=0.3)

    for i,k0s2pi in enumerate(k0s2pis):


        k0 = k0s2pi*2*np.pi
        M = Gd.create_matrix_TE(lattice, Δ_B, Δ_AB, k0)
        N = M.shape[0]
        w, v = np.linalg.eig(M)
            
        idx = w.argsort()[::-1]
        w = w[idx]
        v = v[:, idx]
        ω = 7
    
        f, v = bott.compute_frequencies(w, v)
        kp = np.searchsorted(f, 7)
        U,V = bott.compute_vxvy_w(lattice, v, kp)
        
        uvuv = U@V@np.linalg.inv(U)@np.linalg.inv(V)

        λ_uvuv, _ = np.linalg.eig(uvuv)
        b = np.abs(np.imag(np.round(np.sum(np.log(λ_uvuv))/2/np.pi,3)))
        new_points = np.array(list(zip(np.real(λ_uvuv),np.imag(λ_uvuv))))
        line1s = []
        for j in range(len(current_points)):
            nearest_index = suv.find_nearest_point(current_points[j], new_points)
            line1 = plt.plot([current_points[j, 0], new_points[nearest_index, 0]],
                     [current_points[j, 1], new_points[nearest_index, 1]],
                             color='white',zorder=2)
            line1s.append(line1)

            line2 = plt.plot([current_points[j, 0], new_points[nearest_index, 0]],
                     [current_points[j, 1], new_points[nearest_index, 1]],
                     color='gray',zorder=1)
        current_points = new_points

        plt.text(x=-2.4,y=4.2,s=r"$\Delta_{\bf{B}} = "+f"{Δ_B}$",fontsize=20)
        plt.text(x=-2.4,y=3.3,s=r"$\Delta_{AB} = "+f"{Δ_AB}$",fontsize=20)

        text_b = plt.text(x=0.8,y=4.2,s=f"$B={b}$",fontsize=20)
        text_s = plt.text(x=0.8,y=3.3,s=f"$k_0/2\pi = {np.round(k0s2pi,4)}$",fontsize=20)
        

        sca = plt.scatter(np.real(λ_uvuv), np.imag(λ_uvuv),color="white",s=2,zorder=3)

        plt.axis((-2.5,2.5,-5,5))
        plt.xlabel(r"$\textrm{Re}(\lambda)$",fontsize=20)
        plt.ylabel(r"$\textrm{Im}(\lambda)$",fontsize=20)
        plt.xticks(fontsize=20)
        plt.yticks(fontsize=20)
        plt.tight_layout()
        plt.savefig(f"../anim/eigenvalues_uvuv_{i}.png",format="png",bbox_inches='tight',dpi=300)
        sca.remove()
        for line1 in line1s:
            line1[0].remove()
        text_b.remove()
        text_s.remove()


    # plt.scatter(np.real(λ_uvuv), np.imag(λ_uvuv))
    # plt.show()


    # b = bott.bott(lattice, v, f, omega=7, pol=True, dagger=False, projector=False, verbose=False, vl=None)

    # print(b)
    
        
