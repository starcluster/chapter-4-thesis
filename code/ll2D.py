import numpy as np
import matplotlib.pyplot as plt

import random
import time
from scipy.special import hankel1
from numpy.linalg import eig


def H0(x):
    return hankel1(0, x)  # - 4j/(np.pi*x**2)


def P(x):
    return 1 - 1.0 / x + 1.0 / (x**2)


def Q(x):
    return -1 + 3.0 / x - 3.0 / (x**2)


def H2(x):
    return hankel1(2, x) + 4j / (np.pi * x**2)


def createDisc(R, N):
    """
    Create a disc (radius=R) of N particles randomly spreaded and homogeneous.
    """
    disc = []
    i = 0
    while i < N:
        x = (2 * random.random() - 1) * R
        y = (2 * random.random() - 1) * R
        if x**2 + y**2 < R**2:
            disc.append((x, y))
            i += 1
    disc = np.array(disc)
    return disc


def r(lattice, i, j):
    """
    Distance between sites i and j
    """
    return np.sqrt(
        (lattice[i][1] - lattice[j][1]) ** 2 + (lattice[i][0] - lattice[j][0]) ** 2
    )


def createMatrixScalar(lattice):
    N = lattice.shape[0]
    M = np.zeros((N, N), dtype=complex)
    for i in range(N):
        for j in range(N):
            if i == j:
                M[i, j] = -1 / 2
            else:
                M[i, j] = (-1 / 2) * H0(r(lattice, i, j))
    return M


def createMatrixVector(lattice):
    N = lattice.shape[0]
    print(lattice)
    M = np.zeros((2 * N, 2 * N), dtype=complex)
    for i in range(N):
        for j in range(N):
            if i == j:
                M[2 * i, 2 * j] = 1
                M[2 * i + 1, 2 * j + 1] = 1
            else:
                phi = np.arctan(
                    (lattice[i][1] - lattice[j][1]) / (lattice[i][0] - lattice[j][0])
                )

                M[2 * i, 2 * j] = H0(r(lattice, i, j))
                M[2 * i + 1, 2 * j + 1] = H0(r(lattice, i, j))
                M[2 * i, 2 * j + 1] = -H2(r(lattice, i, j)) * np.exp(-2j * phi)
                M[2 * i + 1, 2 * j] = -H2(r(lattice, i, j)) * np.exp(2j * phi)

                # ce qui suite est à revoir refaire calcul des sommes
                # elles doivent etres differentts pour les termes anti
                # diagonaux

    return M * (-1 / 2)


def IPR(k, v):
    N = v.shape[0]
    h = 0
    b = 0
    for i in range(N):
        h += abs(v[i][k]) ** 4
    for i in range(N):
        b += abs(v[i][k]) ** 2
    return h / (b**2)


def IPRVector(k, v):
    N = v.shape[0]
    h = 0
    b = 0
    for i in range(int(N / 2)):
        t = abs(v[2 * i][k]) ** 2 + abs(v[2 * i + 1][k]) ** 2
        t = t**2
        h += t
    for i in range(N):
        b += abs(v[i][k]) ** 2
    return h / (b**2)


def computeG(omega, gamma):
    N = len(gamma)
    inv_gamma = 0
    omega_sum = 0
    omega_sorted = sorted(omega)
    for i in range(N):
        inv_gamma += 1 / gamma[i]
        if i != (N - 1):
            omega_sum += omega_sorted[i + 1] - omega_sorted[i]
    inv_gamma /= N
    omega_sum /= N - 1

    return 1 / inv_gamma * 1 / omega_sum


def print_matrix(M):
    for i in M:
        for j in i:
            print(np.round(j, 2), end="   ")
        print()


def simulate(N):
    rho = 1
    R = np.sqrt(N / np.pi / rho)

    disc = createDisc(R, N)

    N = disc.shape[0]

    M = createMatrixVector(disc)
    print(M)
    # M = createMatrixScalar(disc)
    w, v = eig(M)
    N = M.shape[0]

    omega = [0] * N
    gamma = [0] * N
    iprs = [0] * N

    for i in range(N):
        omega[i] = -w[i].imag
        gamma[i] = -w[i].real
        iprs[i] = IPRVector(i, v)

    # print_matrix(M)
    #    print(gamma)
    # g = computeG(omega, gamma)

    f = open("vector.dat", "w")
    f.write("x,y,ipr\n")
    for i in range(N):
        f.write(str(omega[i]) + "," + str(gamma[i]) + "," + str(iprs[i]) + "\n")
    f.close()

    plt.scatter(omega, gamma, c=iprs, cmap="jet", marker=".", vmin=0, vmax=0.5)
    plt.colorbar()
    plt.yscale("log")
    plt.xlabel(r"$\omega_n$", fontsize=20)
    plt.ylabel(r"$\gamma_n$", fontsize=20)
    plt.axis((-60, 60, 1e-4, 1e2))
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    simulate(100)

    # t0 = time.time()
    # print(H2_sum(1))
    # t1 = time.time()
    # print(H0_sum_py(1))
    # t2 = time.time()
    # print(t1-t0)
    # print(t2-t1)
    # #simulate(1000)
    # print(H0(0.01))
    # W = 500
    # x = np.linspace(0.1,100,W)
    # #y = np.imag(H0(x*10))*10
    # y = np.zeros(W)
    # y2 = np.zeros(W)
    # d = 1e-2
    # for i in range(W):
    #     y[i] = np.imag(H0(x[i])*1j)
    #     y2[i] = np.imag(H0_sum(x[i]))
    # plt.title("$N = 10^5 \qquad d = 10^{-1}$")
    # plt.plot(x,y, label="hankel")
    # plt.plot(x,y2, label="sum")
    # plt.legend()
    # plt.show()


# plt.scatter( omega, gamma, c=iprs, cmap='jet', marker = '.')
# plt.colorbar()
# plt.yscale('log')
# plt.xlabel(r"$\omega_n$")
# plt.ylabel(r"$\gamma_n$")
# plt.axis((-50,50,1e-13,1e2))

# plt.title("Kernel")
# plt.show()


# plt.plot(disc[:,0], disc[:,1], linestyle='none', marker='+', label = r"$\rho =$ " + str(N/(R**2)), color='red')
# plt.legend()
# plt.show()
